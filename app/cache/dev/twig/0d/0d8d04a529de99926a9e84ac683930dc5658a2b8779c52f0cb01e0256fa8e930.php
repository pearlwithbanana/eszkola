<?php

/* mail/registration.html.twig */
class __TwigTemplate_4efe5cd0a6f5cc481743730da5cfb78d7d73583044500cc9bad8f68e5107a111 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_27279ebab94206e30a408996888870ab8dcc89f31218793dc3bae61b04c8353c = $this->env->getExtension("native_profiler");
        $__internal_27279ebab94206e30a408996888870ab8dcc89f31218793dc3bae61b04c8353c->enter($__internal_27279ebab94206e30a408996888870ab8dcc89f31218793dc3bae61b04c8353c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "mail/registration.html.twig"));

        // line 1
        echo "<h1>Na podany adres email zostało zarejestrowane konto w serwisie elektronicznej szkoły.</h1>

<p>Aby aktywować konto proszę kliknąć w ten link: ";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("user_activation", array("link" => $this->getAttribute((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data")), "hashLink", array()))), "html", null, true);
        echo "</p>

Login do konta: ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data")), "username", array()), "html", null, true);
        
        $__internal_27279ebab94206e30a408996888870ab8dcc89f31218793dc3bae61b04c8353c->leave($__internal_27279ebab94206e30a408996888870ab8dcc89f31218793dc3bae61b04c8353c_prof);

    }

    public function getTemplateName()
    {
        return "mail/registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  26 => 3,  22 => 1,);
    }
}
/* <h1>Na podany adres email zostało zarejestrowane konto w serwisie elektronicznej szkoły.</h1>*/
/* */
/* <p>Aby aktywować konto proszę kliknąć w ten link: {{ url('user_activation', {'link' : data.hashLink}) }}</p>*/
/* */
/* Login do konta: {{ data.username }}*/
