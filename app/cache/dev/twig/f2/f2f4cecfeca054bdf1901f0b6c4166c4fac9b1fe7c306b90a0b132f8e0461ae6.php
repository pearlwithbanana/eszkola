<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_7c1c22cb9e0f02048372ec47b84b864422f5fdc5e1ae222400bc03e66d43c809 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e88e91394bf8bb0809388ff6d4df52c79a3ef431e6cf2d82eb226df9c639f68 = $this->env->getExtension("native_profiler");
        $__internal_5e88e91394bf8bb0809388ff6d4df52c79a3ef431e6cf2d82eb226df9c639f68->enter($__internal_5e88e91394bf8bb0809388ff6d4df52c79a3ef431e6cf2d82eb226df9c639f68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_5e88e91394bf8bb0809388ff6d4df52c79a3ef431e6cf2d82eb226df9c639f68->leave($__internal_5e88e91394bf8bb0809388ff6d4df52c79a3ef431e6cf2d82eb226df9c639f68_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
