<?php

/* index.html.twig */
class __TwigTemplate_208f5cc9c4b299456e7c810dea5b0b5b3a849dbb8c1d1bbe9fe0285323192896 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'stylesheet' => array($this, 'block_stylesheet'),
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f8a3f1e7955fd8bb0141575e77129a6ce16bcb4738722cfb117ed98c7de6fe6 = $this->env->getExtension("native_profiler");
        $__internal_6f8a3f1e7955fd8bb0141575e77129a6ce16bcb4738722cfb117ed98c7de6fe6->enter($__internal_6f8a3f1e7955fd8bb0141575e77129a6ce16bcb4738722cfb117ed98c7de6fe6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f8a3f1e7955fd8bb0141575e77129a6ce16bcb4738722cfb117ed98c7de6fe6->leave($__internal_6f8a3f1e7955fd8bb0141575e77129a6ce16bcb4738722cfb117ed98c7de6fe6_prof);

    }

    // line 2
    public function block_stylesheet($context, array $blocks = array())
    {
        $__internal_1967f81aee6c356febb4f17f69badc17b82ea4013312d13eff8fcf43af610d81 = $this->env->getExtension("native_profiler");
        $__internal_1967f81aee6c356febb4f17f69badc17b82ea4013312d13eff8fcf43af610d81->enter($__internal_1967f81aee6c356febb4f17f69badc17b82ea4013312d13eff8fcf43af610d81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheet"));

        
        $__internal_1967f81aee6c356febb4f17f69badc17b82ea4013312d13eff8fcf43af610d81->leave($__internal_1967f81aee6c356febb4f17f69badc17b82ea4013312d13eff8fcf43af610d81_prof);

    }

    // line 4
    public function block_container($context, array $blocks = array())
    {
        $__internal_f1739aa345f6b5215703643dfe3854d2260e542a0f9030023b3a0fcdae0963b7 = $this->env->getExtension("native_profiler");
        $__internal_f1739aa345f6b5215703643dfe3854d2260e542a0f9030023b3a0fcdae0963b7->enter($__internal_f1739aa345f6b5215703643dfe3854d2260e542a0f9030023b3a0fcdae0963b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 5
        echo "<h1>Aktualnie zalogowano jako: ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "security", array()), "getToken", array(), "method"), "getUser", array(), "method"), "getUsername", array(), "method"), "html", null, true);
        echo " </h1>
</br>
<a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getUrl("logout");
        echo "\">Wyloguj się</a></br>
<a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getUrl("change_password");
        echo "\">Zmień hasło</a></br>
<a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getUrl("remind_password");
        echo "\">Przypomnij hasło</a></br>
<a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getUrl("configuration_step_1");
        echo "\">Dodaj nowych nauczycieli</a></br>
<a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getUrl("configuration_step_2");
        echo "\">Dodaj nowe klasy</a></br>
";
        
        $__internal_f1739aa345f6b5215703643dfe3854d2260e542a0f9030023b3a0fcdae0963b7->leave($__internal_f1739aa345f6b5215703643dfe3854d2260e542a0f9030023b3a0fcdae0963b7_prof);

    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 11,  70 => 10,  66 => 9,  62 => 8,  58 => 7,  52 => 5,  46 => 4,  35 => 2,  11 => 1,);
    }
}
/* {% extends "base.html.twig" %}*/
/* {% block stylesheet %}*/
/* {% endblock %}*/
/* {% block container %}*/
/* <h1>Aktualnie zalogowano jako: {{ app.security.getToken().getUser().getUsername() }} </h1>*/
/* </br>*/
/* <a href="{{ url('logout') }}">Wyloguj się</a></br>*/
/* <a href="{{ url('change_password') }}">Zmień hasło</a></br>*/
/* <a href="{{ url('remind_password') }}">Przypomnij hasło</a></br>*/
/* <a href="{{ url('configuration_step_1') }}">Dodaj nowych nauczycieli</a></br>*/
/* <a href="{{ url('configuration_step_2') }}">Dodaj nowe klasy</a></br>*/
/* {% endblock %}*/
