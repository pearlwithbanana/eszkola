<?php

/* message/link_expired.html.twig */
class __TwigTemplate_2a83b6022c7890359ef97f66d8c77a5aed445e87cf76cf720d35e6b5501be0fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_224539c52e1ee11f1025fa72095ff8a09ed7e0b76bc02b92fd0aa20db3ae0393 = $this->env->getExtension("native_profiler");
        $__internal_224539c52e1ee11f1025fa72095ff8a09ed7e0b76bc02b92fd0aa20db3ae0393->enter($__internal_224539c52e1ee11f1025fa72095ff8a09ed7e0b76bc02b92fd0aa20db3ae0393_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message/link_expired.html.twig"));

        // line 1
        echo "<h1>Link aktywacyjny wygasł lub jest nieprawidłowy. Proszę skontaktować się z administracją. </h1>
<p>Uprzejmie przypominamy, że czas aktywacji konta wynosi 14 dni. </p>";
        
        $__internal_224539c52e1ee11f1025fa72095ff8a09ed7e0b76bc02b92fd0aa20db3ae0393->leave($__internal_224539c52e1ee11f1025fa72095ff8a09ed7e0b76bc02b92fd0aa20db3ae0393_prof);

    }

    public function getTemplateName()
    {
        return "message/link_expired.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <h1>Link aktywacyjny wygasł lub jest nieprawidłowy. Proszę skontaktować się z administracją. </h1>*/
/* <p>Uprzejmie przypominamy, że czas aktywacji konta wynosi 14 dni. </p>*/
