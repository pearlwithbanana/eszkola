<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_5a64a3ed7db2366f60c3654c857982d52706c6a1f9c4d405f5e20a817a63203e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23fda35c14dc5371c43cf4dcd9e8e96fe68df75bb0420f03cc900b09d4b0f1ba = $this->env->getExtension("native_profiler");
        $__internal_23fda35c14dc5371c43cf4dcd9e8e96fe68df75bb0420f03cc900b09d4b0f1ba->enter($__internal_23fda35c14dc5371c43cf4dcd9e8e96fe68df75bb0420f03cc900b09d4b0f1ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_23fda35c14dc5371c43cf4dcd9e8e96fe68df75bb0420f03cc900b09d4b0f1ba->leave($__internal_23fda35c14dc5371c43cf4dcd9e8e96fe68df75bb0420f03cc900b09d4b0f1ba_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
