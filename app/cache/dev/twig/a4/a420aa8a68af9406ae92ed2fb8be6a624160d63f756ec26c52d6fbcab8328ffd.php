<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_c34a80f8ae55fde524c0a74346fe86512aee42fb2af4955234fa44711c1d0c05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_506270319bc309579afedb2948b3e5aa9b7f3f4f776366cd94aad7fefe830fbe = $this->env->getExtension("native_profiler");
        $__internal_506270319bc309579afedb2948b3e5aa9b7f3f4f776366cd94aad7fefe830fbe->enter($__internal_506270319bc309579afedb2948b3e5aa9b7f3f4f776366cd94aad7fefe830fbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_506270319bc309579afedb2948b3e5aa9b7f3f4f776366cd94aad7fefe830fbe->leave($__internal_506270319bc309579afedb2948b3e5aa9b7f3f4f776366cd94aad7fefe830fbe_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
