<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_95a90c4896e7a527f5d93907123c581c5f9c2d6fc3004cb6915eaaa1bba6d2ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28372a502271a80e76e66681063539ce56718926a1161c657229faece29580dd = $this->env->getExtension("native_profiler");
        $__internal_28372a502271a80e76e66681063539ce56718926a1161c657229faece29580dd->enter($__internal_28372a502271a80e76e66681063539ce56718926a1161c657229faece29580dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_28372a502271a80e76e66681063539ce56718926a1161c657229faece29580dd->leave($__internal_28372a502271a80e76e66681063539ce56718926a1161c657229faece29580dd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
