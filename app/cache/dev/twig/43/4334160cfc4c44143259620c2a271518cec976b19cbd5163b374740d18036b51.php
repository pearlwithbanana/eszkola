<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_e776996539dd7d4225ae7c080ec9ce85e4d4a1c6fc7824514101fd297df2b9e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4423b7a4e8b5b1058e981bf9728a587b66de2dae00ad5d1b207c3830b8f9895 = $this->env->getExtension("native_profiler");
        $__internal_c4423b7a4e8b5b1058e981bf9728a587b66de2dae00ad5d1b207c3830b8f9895->enter($__internal_c4423b7a4e8b5b1058e981bf9728a587b66de2dae00ad5d1b207c3830b8f9895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_c4423b7a4e8b5b1058e981bf9728a587b66de2dae00ad5d1b207c3830b8f9895->leave($__internal_c4423b7a4e8b5b1058e981bf9728a587b66de2dae00ad5d1b207c3830b8f9895_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
