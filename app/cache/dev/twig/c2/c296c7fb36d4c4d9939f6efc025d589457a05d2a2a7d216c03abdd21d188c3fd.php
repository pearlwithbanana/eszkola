<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_df4341661fee1f7e7ea1f602a240ece653cb07551fe646d392cd7da64cbfb7a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc51958570838ef0e61bf148fd8c22f9554a9d79a65976f672c9710dc411878f = $this->env->getExtension("native_profiler");
        $__internal_bc51958570838ef0e61bf148fd8c22f9554a9d79a65976f672c9710dc411878f->enter($__internal_bc51958570838ef0e61bf148fd8c22f9554a9d79a65976f672c9710dc411878f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_bc51958570838ef0e61bf148fd8c22f9554a9d79a65976f672c9710dc411878f->leave($__internal_bc51958570838ef0e61bf148fd8c22f9554a9d79a65976f672c9710dc411878f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
