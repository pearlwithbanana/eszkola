<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_633d8080c8292a8f883d3dad07dd3470062ac6283603b2d53bfda066b007cd1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b3fb096bb453663d89545f6c848648ce57a19db8a2215fe5454b565c4d8e773 = $this->env->getExtension("native_profiler");
        $__internal_3b3fb096bb453663d89545f6c848648ce57a19db8a2215fe5454b565c4d8e773->enter($__internal_3b3fb096bb453663d89545f6c848648ce57a19db8a2215fe5454b565c4d8e773_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_3b3fb096bb453663d89545f6c848648ce57a19db8a2215fe5454b565c4d8e773->leave($__internal_3b3fb096bb453663d89545f6c848648ce57a19db8a2215fe5454b565c4d8e773_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
