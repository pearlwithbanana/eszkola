<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_488e3a3a6e41ff0309c693a00114eb4019d5eced043e4f41a316558870e3c4e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0c052664c01d88841f82506d2b1244f84f37f48f46eb584d92ac2e284fb020b = $this->env->getExtension("native_profiler");
        $__internal_f0c052664c01d88841f82506d2b1244f84f37f48f46eb584d92ac2e284fb020b->enter($__internal_f0c052664c01d88841f82506d2b1244f84f37f48f46eb584d92ac2e284fb020b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f0c052664c01d88841f82506d2b1244f84f37f48f46eb584d92ac2e284fb020b->leave($__internal_f0c052664c01d88841f82506d2b1244f84f37f48f46eb584d92ac2e284fb020b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_634e2f2fa1648eab5e204cfa8954def51118751535fe0e5aa9395f1d569e1055 = $this->env->getExtension("native_profiler");
        $__internal_634e2f2fa1648eab5e204cfa8954def51118751535fe0e5aa9395f1d569e1055->enter($__internal_634e2f2fa1648eab5e204cfa8954def51118751535fe0e5aa9395f1d569e1055_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_634e2f2fa1648eab5e204cfa8954def51118751535fe0e5aa9395f1d569e1055->leave($__internal_634e2f2fa1648eab5e204cfa8954def51118751535fe0e5aa9395f1d569e1055_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_962ae342779e352110eece2e7a6daec045184f2e51a1e36f56215bfe1b1c205b = $this->env->getExtension("native_profiler");
        $__internal_962ae342779e352110eece2e7a6daec045184f2e51a1e36f56215bfe1b1c205b->enter($__internal_962ae342779e352110eece2e7a6daec045184f2e51a1e36f56215bfe1b1c205b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_962ae342779e352110eece2e7a6daec045184f2e51a1e36f56215bfe1b1c205b->leave($__internal_962ae342779e352110eece2e7a6daec045184f2e51a1e36f56215bfe1b1c205b_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
