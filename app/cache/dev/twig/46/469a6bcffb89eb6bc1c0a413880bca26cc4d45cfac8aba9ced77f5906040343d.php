<?php

/* message/password_changed.html.twig */
class __TwigTemplate_ff192eda1dbad9613bd5ebf45f5790884322736f831135d08dc30e024731e117 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c281f6ebdbc016bd1114d60aaca8de09a17dd6584a3407a3b51d6d20d476e99 = $this->env->getExtension("native_profiler");
        $__internal_9c281f6ebdbc016bd1114d60aaca8de09a17dd6584a3407a3b51d6d20d476e99->enter($__internal_9c281f6ebdbc016bd1114d60aaca8de09a17dd6584a3407a3b51d6d20d476e99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message/password_changed.html.twig"));

        // line 1
        echo "<meta http-equiv=\"refresh\" content=\"7;";
        echo $this->env->getExtension('routing')->getUrl("login");
        echo "\" />
<h1>Hasło zostało zmienione. Za chwilę nastąpi przekierowanie na strunę logowania.</h1>
<p>Jeśli nie chcesz czekać, kliknij w ten <a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getUrl("login");
        echo "\">link</a></p>";
        
        $__internal_9c281f6ebdbc016bd1114d60aaca8de09a17dd6584a3407a3b51d6d20d476e99->leave($__internal_9c281f6ebdbc016bd1114d60aaca8de09a17dd6584a3407a3b51d6d20d476e99_prof);

    }

    public function getTemplateName()
    {
        return "message/password_changed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <meta http-equiv="refresh" content="7;{{ url('login') }}" />*/
/* <h1>Hasło zostało zmienione. Za chwilę nastąpi przekierowanie na strunę logowania.</h1>*/
/* <p>Jeśli nie chcesz czekać, kliknij w ten <a href="{{ url('login') }}">link</a></p>*/
