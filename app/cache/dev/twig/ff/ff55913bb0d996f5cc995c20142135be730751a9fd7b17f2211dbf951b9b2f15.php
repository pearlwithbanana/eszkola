<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_91530ee4e1eebfc5c29d4fe424c5f15e27922ae471bd8bdfd1a5eeae4fa1020c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d67a74f332ae8efed10ded3988ea846bb8cfdffdeb7c24b5c1eee48d431be99 = $this->env->getExtension("native_profiler");
        $__internal_6d67a74f332ae8efed10ded3988ea846bb8cfdffdeb7c24b5c1eee48d431be99->enter($__internal_6d67a74f332ae8efed10ded3988ea846bb8cfdffdeb7c24b5c1eee48d431be99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_6d67a74f332ae8efed10ded3988ea846bb8cfdffdeb7c24b5c1eee48d431be99->leave($__internal_6d67a74f332ae8efed10ded3988ea846bb8cfdffdeb7c24b5c1eee48d431be99_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
