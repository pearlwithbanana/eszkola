<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_b10fdd7576158ef36afd1aea734e6634d1dbf71b11c6a83891b80804698e2d07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_94023e24ed426be235f94fda1476aae99f8431b8a966c36ef5edfb9ce8763daf = $this->env->getExtension("native_profiler");
        $__internal_94023e24ed426be235f94fda1476aae99f8431b8a966c36ef5edfb9ce8763daf->enter($__internal_94023e24ed426be235f94fda1476aae99f8431b8a966c36ef5edfb9ce8763daf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_94023e24ed426be235f94fda1476aae99f8431b8a966c36ef5edfb9ce8763daf->leave($__internal_94023e24ed426be235f94fda1476aae99f8431b8a966c36ef5edfb9ce8763daf_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
