<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_0c718d35a1fceef4ad1bcd19f94cb8916fb17b049fff820111c617f77e370b64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_107665fc742a5574b49d41ff8c7435d63f3fd06a5c79e1d9cdfcbe1cf580f6f4 = $this->env->getExtension("native_profiler");
        $__internal_107665fc742a5574b49d41ff8c7435d63f3fd06a5c79e1d9cdfcbe1cf580f6f4->enter($__internal_107665fc742a5574b49d41ff8c7435d63f3fd06a5c79e1d9cdfcbe1cf580f6f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_107665fc742a5574b49d41ff8c7435d63f3fd06a5c79e1d9cdfcbe1cf580f6f4->leave($__internal_107665fc742a5574b49d41ff8c7435d63f3fd06a5c79e1d9cdfcbe1cf580f6f4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
