<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_38b9e1b750e6bcfa8dee643d08d4f0adfd9f40a1a3283f77ec67f5d709b4edf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf3013202a48eda5b94f7a487afd6abf260fd89fec6f2208edca690e847fd718 = $this->env->getExtension("native_profiler");
        $__internal_cf3013202a48eda5b94f7a487afd6abf260fd89fec6f2208edca690e847fd718->enter($__internal_cf3013202a48eda5b94f7a487afd6abf260fd89fec6f2208edca690e847fd718_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_cf3013202a48eda5b94f7a487afd6abf260fd89fec6f2208edca690e847fd718->leave($__internal_cf3013202a48eda5b94f7a487afd6abf260fd89fec6f2208edca690e847fd718_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
