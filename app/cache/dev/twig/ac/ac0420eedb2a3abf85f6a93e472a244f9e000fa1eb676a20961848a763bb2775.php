<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_306fffa27e7596c4657d0541f2b242b47b81c6c3900803ba1174bb091c6bf5e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e2d80ff361bb4e2f8456b2318c6a4a18ddceace02e6d0a07cf57434a0611b47 = $this->env->getExtension("native_profiler");
        $__internal_4e2d80ff361bb4e2f8456b2318c6a4a18ddceace02e6d0a07cf57434a0611b47->enter($__internal_4e2d80ff361bb4e2f8456b2318c6a4a18ddceace02e6d0a07cf57434a0611b47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_4e2d80ff361bb4e2f8456b2318c6a4a18ddceace02e6d0a07cf57434a0611b47->leave($__internal_4e2d80ff361bb4e2f8456b2318c6a4a18ddceace02e6d0a07cf57434a0611b47_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
