<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_402e57b2887bee5d5ecd94027c027885195232cd61ea477019c4e65b9670ed96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad7da846a63d7d27e4f79a92c931a7f02fca419e2ee1058c27003c51d8d77ef3 = $this->env->getExtension("native_profiler");
        $__internal_ad7da846a63d7d27e4f79a92c931a7f02fca419e2ee1058c27003c51d8d77ef3->enter($__internal_ad7da846a63d7d27e4f79a92c931a7f02fca419e2ee1058c27003c51d8d77ef3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_ad7da846a63d7d27e4f79a92c931a7f02fca419e2ee1058c27003c51d8d77ef3->leave($__internal_ad7da846a63d7d27e4f79a92c931a7f02fca419e2ee1058c27003c51d8d77ef3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
