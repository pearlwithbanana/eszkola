<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_8ed718e3981bda3f5742735cb6c4fd7096bec8bea1e341f2689aa399991ebe10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d21acdf82124a8fe812f61c0ecfa64154d20ded4c62fb089706214315201d2da = $this->env->getExtension("native_profiler");
        $__internal_d21acdf82124a8fe812f61c0ecfa64154d20ded4c62fb089706214315201d2da->enter($__internal_d21acdf82124a8fe812f61c0ecfa64154d20ded4c62fb089706214315201d2da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_d21acdf82124a8fe812f61c0ecfa64154d20ded4c62fb089706214315201d2da->leave($__internal_d21acdf82124a8fe812f61c0ecfa64154d20ded4c62fb089706214315201d2da_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
