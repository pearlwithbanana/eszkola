<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_6ab270ee20da8102d969b6d9b5dda4a2f25a3c524aab94b2d2e88643014e0b6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44759a7ce70de7054b07cb776916c7260f7ccf38c12958f5dfc6010e7d483338 = $this->env->getExtension("native_profiler");
        $__internal_44759a7ce70de7054b07cb776916c7260f7ccf38c12958f5dfc6010e7d483338->enter($__internal_44759a7ce70de7054b07cb776916c7260f7ccf38c12958f5dfc6010e7d483338_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_44759a7ce70de7054b07cb776916c7260f7ccf38c12958f5dfc6010e7d483338->leave($__internal_44759a7ce70de7054b07cb776916c7260f7ccf38c12958f5dfc6010e7d483338_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
