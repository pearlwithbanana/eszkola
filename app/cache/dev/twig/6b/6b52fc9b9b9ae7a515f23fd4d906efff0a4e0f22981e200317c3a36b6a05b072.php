<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_569ffdf90e2b6b2e477985379b8f1f8965af66ad4883251a8cd3de4fdb9d5b6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b6f9b78b6e3906550a40d7e3e8ab149dd065c474f568b4f434818e0dc19eff8 = $this->env->getExtension("native_profiler");
        $__internal_5b6f9b78b6e3906550a40d7e3e8ab149dd065c474f568b4f434818e0dc19eff8->enter($__internal_5b6f9b78b6e3906550a40d7e3e8ab149dd065c474f568b4f434818e0dc19eff8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_5b6f9b78b6e3906550a40d7e3e8ab149dd065c474f568b4f434818e0dc19eff8->leave($__internal_5b6f9b78b6e3906550a40d7e3e8ab149dd065c474f568b4f434818e0dc19eff8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
