<?php

/* form/login.html.twig */
class __TwigTemplate_a65fd9c3fdc67ee1b8effffec2d4523257c1175d4ac1c54ff2a7d6037396538a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_97c2adda6066866dc735965a6a74f0c2f58829497d86cd4a93a654efebf8d147 = $this->env->getExtension("native_profiler");
        $__internal_97c2adda6066866dc735965a6a74f0c2f58829497d86cd4a93a654efebf8d147->enter($__internal_97c2adda6066866dc735965a6a74f0c2f58829497d86cd4a93a654efebf8d147_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/login.html.twig"));

        // line 1
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 2
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 4
        echo "
<form action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
    <label for=\"username\">Login:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />

    <label for=\"password\">Hasło:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    ";
        // line 17
        echo "
    <button type=\"submit\">Zaloguj się</button>
</form>";
        
        $__internal_97c2adda6066866dc735965a6a74f0c2f58829497d86cd4a93a654efebf8d147->leave($__internal_97c2adda6066866dc735965a6a74f0c2f58829497d86cd4a93a654efebf8d147_prof);

    }

    public function getTemplateName()
    {
        return "form/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 17,  38 => 7,  33 => 5,  30 => 4,  24 => 2,  22 => 1,);
    }
}
/* {% if error %}*/
/*     <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/* {% endif %}*/
/* */
/* <form action="{{ path('login_check') }}" method="post">*/
/*     <label for="username">Login:</label>*/
/*     <input type="text" id="username" name="_username" value="{{ last_username }}" />*/
/* */
/*     <label for="password">Hasło:</label>*/
/*     <input type="password" id="password" name="_password" />*/
/* */
/*     {#*/
/*         If you want to control the URL the user*/
/*         is redirected to on success (more details below)*/
/*         <input type="hidden" name="_target_path" value="/account" />*/
/*     #}*/
/* */
/*     <button type="submit">Zaloguj się</button>*/
/* </form>*/
