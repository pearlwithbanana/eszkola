<?php

/* message/success_activation.html.twig */
class __TwigTemplate_afc54c928b185550e314b944c503b9a69fa29f8bc74f00eae561a297498d9cf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98bce9115b435dd966eb3c91993b53d3395a686347776988ad19821657a2173a = $this->env->getExtension("native_profiler");
        $__internal_98bce9115b435dd966eb3c91993b53d3395a686347776988ad19821657a2173a->enter($__internal_98bce9115b435dd966eb3c91993b53d3395a686347776988ad19821657a2173a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message/success_activation.html.twig"));

        // line 1
        echo "Konto ";
        echo twig_escape_filter($this->env, (isset($context["username"]) ? $context["username"] : $this->getContext($context, "username")), "html", null, true);
        echo " zostało aktywowane.";
        
        $__internal_98bce9115b435dd966eb3c91993b53d3395a686347776988ad19821657a2173a->leave($__internal_98bce9115b435dd966eb3c91993b53d3395a686347776988ad19821657a2173a_prof);

    }

    public function getTemplateName()
    {
        return "message/success_activation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* Konto {{ username }} zostało aktywowane.*/
