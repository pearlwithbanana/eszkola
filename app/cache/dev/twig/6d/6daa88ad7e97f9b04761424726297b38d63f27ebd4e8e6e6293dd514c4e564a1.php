<?php

/* mail/remind_password.html.twig */
class __TwigTemplate_633f3b3417034a0c9001f5be3504c4390c9178d5ef531e1ce62282b73f587efd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eb31b8983396fe45b19b6b070c51f4dfb903164736a82192d2447646b0b89c0a = $this->env->getExtension("native_profiler");
        $__internal_eb31b8983396fe45b19b6b070c51f4dfb903164736a82192d2447646b0b89c0a->enter($__internal_eb31b8983396fe45b19b6b070c51f4dfb903164736a82192d2447646b0b89c0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "mail/remind_password.html.twig"));

        // line 1
        echo "<h1>Dzień dobry</h1>
<p>
    Doszły nas słuchy, że dla konta ";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data")), "username", array()), "html", null, true);
        echo " zapomniano hasła.
    Jeśli to nie Ty prosiłaś/prosiłeś o przypomnienie hasła zignoruj tę wiadomość.
</br>
    W przeciwnym wypadku kliknij w poniższy odnośnik, który przekieruje Cię
    do strony, na której zmienisz swoje hasło na nowe.

    Uprzejmie informujemy jednocześnie, że link wygaśnie w ciągu 14 dni.

    ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("new_password", array("link" => $this->getAttribute((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data")), "mailHash", array()))), "html", null, true);
        echo "
</p>";
        
        $__internal_eb31b8983396fe45b19b6b070c51f4dfb903164736a82192d2447646b0b89c0a->leave($__internal_eb31b8983396fe45b19b6b070c51f4dfb903164736a82192d2447646b0b89c0a_prof);

    }

    public function getTemplateName()
    {
        return "mail/remind_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 11,  26 => 3,  22 => 1,);
    }
}
/* <h1>Dzień dobry</h1>*/
/* <p>*/
/*     Doszły nas słuchy, że dla konta {{ data.username }} zapomniano hasła.*/
/*     Jeśli to nie Ty prosiłaś/prosiłeś o przypomnienie hasła zignoruj tę wiadomość.*/
/* </br>*/
/*     W przeciwnym wypadku kliknij w poniższy odnośnik, który przekieruje Cię*/
/*     do strony, na której zmienisz swoje hasło na nowe.*/
/* */
/*     Uprzejmie informujemy jednocześnie, że link wygaśnie w ciągu 14 dni.*/
/* */
/*     {{ url('new_password', {'link' : data.mailHash} ) }}*/
/* </p>*/
