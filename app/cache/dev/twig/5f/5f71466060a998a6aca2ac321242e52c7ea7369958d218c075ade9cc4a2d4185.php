<?php

/* mail/new_password.html.twig */
class __TwigTemplate_8bea73d0c6904010cc06ce958e7a2a7b1a38407af9b77534597d2086b7e236e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c50d45418f40a4727a0df8bb2f885b95a5cd32708b4a9e14cf79c7227c80536 = $this->env->getExtension("native_profiler");
        $__internal_0c50d45418f40a4727a0df8bb2f885b95a5cd32708b4a9e14cf79c7227c80536->enter($__internal_0c50d45418f40a4727a0df8bb2f885b95a5cd32708b4a9e14cf79c7227c80536_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "mail/new_password.html.twig"));

        // line 1
        echo "<h1>Zmiana hasła dla użytkownika ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data")), "username", array()), "html", null, true);
        echo " w serwisie elektronicznej szkoły:</h1>

<p>Aby zmienić hasło proszę kliknąć w podany link i postępować zgodnie z wskazówkami:
    ";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("new_password", array("link" => $this->getAttribute((isset($context["data"]) ? $context["data"] : $this->getContext($context, "data")), "hashLink", array()))), "html", null, true);
        echo "</p>
";
        
        $__internal_0c50d45418f40a4727a0df8bb2f885b95a5cd32708b4a9e14cf79c7227c80536->leave($__internal_0c50d45418f40a4727a0df8bb2f885b95a5cd32708b4a9e14cf79c7227c80536_prof);

    }

    public function getTemplateName()
    {
        return "mail/new_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  22 => 1,);
    }
}
/* <h1>Zmiana hasła dla użytkownika {{ data.username }} w serwisie elektronicznej szkoły:</h1>*/
/* */
/* <p>Aby zmienić hasło proszę kliknąć w podany link i postępować zgodnie z wskazówkami:*/
/*     {{ url('new_password', {'link' : data.hashLink}) }}</p>*/
/* */
