<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_497f6aecf534fd41366c7859e511df4479eb8eedfa859e117d35db65a1f4751f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7aaf411dbbe6312fa050dc297cea707c9dc12f959c0770a8d71bb17a2b999c63 = $this->env->getExtension("native_profiler");
        $__internal_7aaf411dbbe6312fa050dc297cea707c9dc12f959c0770a8d71bb17a2b999c63->enter($__internal_7aaf411dbbe6312fa050dc297cea707c9dc12f959c0770a8d71bb17a2b999c63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_7aaf411dbbe6312fa050dc297cea707c9dc12f959c0770a8d71bb17a2b999c63->leave($__internal_7aaf411dbbe6312fa050dc297cea707c9dc12f959c0770a8d71bb17a2b999c63_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
