<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_603cfa15545980dd5b44068fb8a9d08e6d23f25fed81e4ed9be5941eac2b47eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f23a77b94da0e34c70d325f4227d043b188b441c50fcb67d9a408c07d47de60 = $this->env->getExtension("native_profiler");
        $__internal_6f23a77b94da0e34c70d325f4227d043b188b441c50fcb67d9a408c07d47de60->enter($__internal_6f23a77b94da0e34c70d325f4227d043b188b441c50fcb67d9a408c07d47de60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_6f23a77b94da0e34c70d325f4227d043b188b441c50fcb67d9a408c07d47de60->leave($__internal_6f23a77b94da0e34c70d325f4227d043b188b441c50fcb67d9a408c07d47de60_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
