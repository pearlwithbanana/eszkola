<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_b172b778723e3cba634f6e269c9c1ccc3f2c0d541eb4fa98cbe24c6aa0829e8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98089a3370412ef44d4d2961c8829d98ffc29395ad46fc283f1f90dc52231f03 = $this->env->getExtension("native_profiler");
        $__internal_98089a3370412ef44d4d2961c8829d98ffc29395ad46fc283f1f90dc52231f03->enter($__internal_98089a3370412ef44d4d2961c8829d98ffc29395ad46fc283f1f90dc52231f03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_98089a3370412ef44d4d2961c8829d98ffc29395ad46fc283f1f90dc52231f03->leave($__internal_98089a3370412ef44d4d2961c8829d98ffc29395ad46fc283f1f90dc52231f03_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
