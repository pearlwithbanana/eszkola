<?php

/* message/success_password_remind.html.twig */
class __TwigTemplate_c48109133cdbd3d2f8ae6bcb3fcd683b15b6945bdaae6a0731e261ddcc65960f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49fdc9a865dab3a6c43b65f201278b45fde10902bbde5e3f660a85a5e9d0b219 = $this->env->getExtension("native_profiler");
        $__internal_49fdc9a865dab3a6c43b65f201278b45fde10902bbde5e3f660a85a5e9d0b219->enter($__internal_49fdc9a865dab3a6c43b65f201278b45fde10902bbde5e3f660a85a5e9d0b219_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message/success_password_remind.html.twig"));

        // line 1
        echo "<h1>
    Na adres ";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo " została wysłana wiadomość zawierająca link do strony z formularzem zmiany hasła.
    Zaloguj się na pocztę i kliknij w wspomniany odnośnik.
</h1>";
        
        $__internal_49fdc9a865dab3a6c43b65f201278b45fde10902bbde5e3f660a85a5e9d0b219->leave($__internal_49fdc9a865dab3a6c43b65f201278b45fde10902bbde5e3f660a85a5e9d0b219_prof);

    }

    public function getTemplateName()
    {
        return "message/success_password_remind.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* <h1>*/
/*     Na adres {{ email }} została wysłana wiadomość zawierająca link do strony z formularzem zmiany hasła.*/
/*     Zaloguj się na pocztę i kliknij w wspomniany odnośnik.*/
/* </h1>*/
