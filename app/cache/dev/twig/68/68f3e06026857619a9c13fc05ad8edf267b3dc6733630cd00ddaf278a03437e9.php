<?php

/* form/remind_password.html.twig */
class __TwigTemplate_aeda3cebd6407638e040ed7c93a33f2db6f466f0bfa4fa56ffa59594b449b231 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca002b55f83b30218d4accbab6b69d8bef9280157acbcef7dd300c127f34a48a = $this->env->getExtension("native_profiler");
        $__internal_ca002b55f83b30218d4accbab6b69d8bef9280157acbcef7dd300c127f34a48a->enter($__internal_ca002b55f83b30218d4accbab6b69d8bef9280157acbcef7dd300c127f34a48a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/remind_password.html.twig"));

        // line 1
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 2
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailOrLogin", array()), 'row');
        echo "
";
        // line 4
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        
        $__internal_ca002b55f83b30218d4accbab6b69d8bef9280157acbcef7dd300c127f34a48a->leave($__internal_ca002b55f83b30218d4accbab6b69d8bef9280157acbcef7dd300c127f34a48a_prof);

    }

    public function getTemplateName()
    {
        return "form/remind_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 4,  30 => 3,  26 => 2,  22 => 1,);
    }
}
/* {{ form_start(form) }}*/
/* {{ form_errors(form) }}*/
/* {{ form_row(form.emailOrLogin) }}*/
/* {{ form_end(form) }}*/
