<?php

/* form/change_password.html.twig */
class __TwigTemplate_65127a9106e143e5e4c5952033567bebf772108451695f1c75ba09acefea6a3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce9b75a75c3dd1a6e94ce6a060e00304e5e5f7463326daf6980e39bcc61a8306 = $this->env->getExtension("native_profiler");
        $__internal_ce9b75a75c3dd1a6e94ce6a060e00304e5e5f7463326daf6980e39bcc61a8306->enter($__internal_ce9b75a75c3dd1a6e94ce6a060e00304e5e5f7463326daf6980e39bcc61a8306_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/change_password.html.twig"));

        // line 1
        echo "<input type='button'value='Rozpocznij procedurę zmiany hasła' class=\"btn\" onclick=\"document.location.href='";
        echo $this->env->getExtension('routing')->getUrl("change_password_checker");
        echo "'\"/>";
        
        $__internal_ce9b75a75c3dd1a6e94ce6a060e00304e5e5f7463326daf6980e39bcc61a8306->leave($__internal_ce9b75a75c3dd1a6e94ce6a060e00304e5e5f7463326daf6980e39bcc61a8306_prof);

    }

    public function getTemplateName()
    {
        return "form/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type='button'value='Rozpocznij procedurę zmiany hasła' class="btn" onclick="document.location.href='{{ url('change_password_checker') }}'"/>*/
