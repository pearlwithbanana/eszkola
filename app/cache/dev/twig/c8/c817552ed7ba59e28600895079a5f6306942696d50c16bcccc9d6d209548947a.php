<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_c7874ec0dfc206e52dd45c597c02d55baac288a651bb290860cc46c69d9466e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5f4147413d390348e53ae73c53fdca8865a18999b91fc088900c542b3ebcd5d = $this->env->getExtension("native_profiler");
        $__internal_e5f4147413d390348e53ae73c53fdca8865a18999b91fc088900c542b3ebcd5d->enter($__internal_e5f4147413d390348e53ae73c53fdca8865a18999b91fc088900c542b3ebcd5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_e5f4147413d390348e53ae73c53fdca8865a18999b91fc088900c542b3ebcd5d->leave($__internal_e5f4147413d390348e53ae73c53fdca8865a18999b91fc088900c542b3ebcd5d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
