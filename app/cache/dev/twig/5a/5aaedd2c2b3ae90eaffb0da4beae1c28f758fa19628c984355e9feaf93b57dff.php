<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_115d26facf62bbea7ac8e29580335f28f27c292885d62f9f4c6b233c33303647 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7c3f50e2b36e96b8a1d49b46717adc41c8349b6133d8331488f7102af51f927 = $this->env->getExtension("native_profiler");
        $__internal_e7c3f50e2b36e96b8a1d49b46717adc41c8349b6133d8331488f7102af51f927->enter($__internal_e7c3f50e2b36e96b8a1d49b46717adc41c8349b6133d8331488f7102af51f927_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_e7c3f50e2b36e96b8a1d49b46717adc41c8349b6133d8331488f7102af51f927->leave($__internal_e7c3f50e2b36e96b8a1d49b46717adc41c8349b6133d8331488f7102af51f927_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
