<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_54fb37ddf4be0ce18d28b147bb6e3b7fed01065b5b3769145e3e45f67055c374 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33a84c657e5ecda79817abfdcb56376ea6e1bb009880176eff9917748a861a72 = $this->env->getExtension("native_profiler");
        $__internal_33a84c657e5ecda79817abfdcb56376ea6e1bb009880176eff9917748a861a72->enter($__internal_33a84c657e5ecda79817abfdcb56376ea6e1bb009880176eff9917748a861a72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_33a84c657e5ecda79817abfdcb56376ea6e1bb009880176eff9917748a861a72->leave($__internal_33a84c657e5ecda79817abfdcb56376ea6e1bb009880176eff9917748a861a72_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
