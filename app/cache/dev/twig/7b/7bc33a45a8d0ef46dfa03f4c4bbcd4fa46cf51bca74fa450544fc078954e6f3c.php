<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_286ae76658aa0a7cf9ae6db77168c5756e7fdf94c58721e64f9952df7c6df105 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_632eb5afcc193f000472db16fd9fd9440fecb77cdff9ece86f70da888bc6d990 = $this->env->getExtension("native_profiler");
        $__internal_632eb5afcc193f000472db16fd9fd9440fecb77cdff9ece86f70da888bc6d990->enter($__internal_632eb5afcc193f000472db16fd9fd9440fecb77cdff9ece86f70da888bc6d990_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_632eb5afcc193f000472db16fd9fd9440fecb77cdff9ece86f70da888bc6d990->leave($__internal_632eb5afcc193f000472db16fd9fd9440fecb77cdff9ece86f70da888bc6d990_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
