<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_a1c6ed331fe3977a16dcb79ebbcb2f708d198fa965df8f82e2165b0181d9e172 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13bbc3d3199c30c8defd2af4bde90f738e433afdc950ba3e0e209e007a686cb3 = $this->env->getExtension("native_profiler");
        $__internal_13bbc3d3199c30c8defd2af4bde90f738e433afdc950ba3e0e209e007a686cb3->enter($__internal_13bbc3d3199c30c8defd2af4bde90f738e433afdc950ba3e0e209e007a686cb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_13bbc3d3199c30c8defd2af4bde90f738e433afdc950ba3e0e209e007a686cb3->leave($__internal_13bbc3d3199c30c8defd2af4bde90f738e433afdc950ba3e0e209e007a686cb3_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_977944b73362278484d7b52b112554195ff552095740acb248a15c72dfb33c86 = $this->env->getExtension("native_profiler");
        $__internal_977944b73362278484d7b52b112554195ff552095740acb248a15c72dfb33c86->enter($__internal_977944b73362278484d7b52b112554195ff552095740acb248a15c72dfb33c86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_977944b73362278484d7b52b112554195ff552095740acb248a15c72dfb33c86->leave($__internal_977944b73362278484d7b52b112554195ff552095740acb248a15c72dfb33c86_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
