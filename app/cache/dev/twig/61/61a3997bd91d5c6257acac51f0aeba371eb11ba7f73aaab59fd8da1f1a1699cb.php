<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_fe82cdaa352f100574e1496b005900dbbac703dbe092f4dbc8c308a36ebf5daf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2e074b20243a6dbc584e7b3b6eed052f875467f0b8f7f9d9161d5f41c5a6d73 = $this->env->getExtension("native_profiler");
        $__internal_d2e074b20243a6dbc584e7b3b6eed052f875467f0b8f7f9d9161d5f41c5a6d73->enter($__internal_d2e074b20243a6dbc584e7b3b6eed052f875467f0b8f7f9d9161d5f41c5a6d73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_d2e074b20243a6dbc584e7b3b6eed052f875467f0b8f7f9d9161d5f41c5a6d73->leave($__internal_d2e074b20243a6dbc584e7b3b6eed052f875467f0b8f7f9d9161d5f41c5a6d73_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
