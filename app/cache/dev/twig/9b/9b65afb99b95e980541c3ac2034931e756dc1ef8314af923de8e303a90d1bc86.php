<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_499994ad9413894182d38d043750e9b0ed68c93e6d43b8baa475f6e96be2f2d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87519995eee378de23813585a12e6f511a328a66e525df54171005378dad102c = $this->env->getExtension("native_profiler");
        $__internal_87519995eee378de23813585a12e6f511a328a66e525df54171005378dad102c->enter($__internal_87519995eee378de23813585a12e6f511a328a66e525df54171005378dad102c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_87519995eee378de23813585a12e6f511a328a66e525df54171005378dad102c->leave($__internal_87519995eee378de23813585a12e6f511a328a66e525df54171005378dad102c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
