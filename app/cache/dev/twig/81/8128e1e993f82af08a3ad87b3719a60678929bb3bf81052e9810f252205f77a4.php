<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_ddbe4a78143abe4b5c826a7061500e6eaed79debeeeaf842855783eccb81b8ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_10f61423099e99997bb4107587038e2e6c0d41f48ff1c0e273803ebbbd41ec5d = $this->env->getExtension("native_profiler");
        $__internal_10f61423099e99997bb4107587038e2e6c0d41f48ff1c0e273803ebbbd41ec5d->enter($__internal_10f61423099e99997bb4107587038e2e6c0d41f48ff1c0e273803ebbbd41ec5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_10f61423099e99997bb4107587038e2e6c0d41f48ff1c0e273803ebbbd41ec5d->leave($__internal_10f61423099e99997bb4107587038e2e6c0d41f48ff1c0e273803ebbbd41ec5d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
