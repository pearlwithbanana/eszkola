<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_71e53a12c2ddbe950786c59c10860b54b9657cf84d70562642c793a03a6b0ce3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83da71668a43653495274837d9fc4203d462610dca28bf2bca4f95761ff79dfc = $this->env->getExtension("native_profiler");
        $__internal_83da71668a43653495274837d9fc4203d462610dca28bf2bca4f95761ff79dfc->enter($__internal_83da71668a43653495274837d9fc4203d462610dca28bf2bca4f95761ff79dfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_83da71668a43653495274837d9fc4203d462610dca28bf2bca4f95761ff79dfc->leave($__internal_83da71668a43653495274837d9fc4203d462610dca28bf2bca4f95761ff79dfc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
