<?php

/* message/success_password_change.html.twig */
class __TwigTemplate_00f701411aee09fa93a0512ccbf30895ca05ef1d10d64eb68382be65abbb6486 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_124c2f251e4819006c8b9d242a1d10eef2e839e7a70dba1201fa7e3a31dc0254 = $this->env->getExtension("native_profiler");
        $__internal_124c2f251e4819006c8b9d242a1d10eef2e839e7a70dba1201fa7e3a31dc0254->enter($__internal_124c2f251e4819006c8b9d242a1d10eef2e839e7a70dba1201fa7e3a31dc0254_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message/success_password_change.html.twig"));

        // line 1
        echo "<h1>Na przypisany do konta adres email wysłano instrukcje odnośnie zmiany hasła.</h1>
<p>Aby kontynuować procedurę, zaloguj się na swoją pocztę i postępuj zgodnie z wskazówkami.</p>";
        
        $__internal_124c2f251e4819006c8b9d242a1d10eef2e839e7a70dba1201fa7e3a31dc0254->leave($__internal_124c2f251e4819006c8b9d242a1d10eef2e839e7a70dba1201fa7e3a31dc0254_prof);

    }

    public function getTemplateName()
    {
        return "message/success_password_change.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <h1>Na przypisany do konta adres email wysłano instrukcje odnośnie zmiany hasła.</h1>*/
/* <p>Aby kontynuować procedurę, zaloguj się na swoją pocztę i postępuj zgodnie z wskazówkami.</p>*/
