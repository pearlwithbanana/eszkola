<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_6b1a176681b9e5b90771925fa11b269e1186e35e5c0a5ca467a46b50bf23e3f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d84369d6a309540f49e6739510c1d7dbb1fab6de65a5e8f8345a3cff103d6ce = $this->env->getExtension("native_profiler");
        $__internal_5d84369d6a309540f49e6739510c1d7dbb1fab6de65a5e8f8345a3cff103d6ce->enter($__internal_5d84369d6a309540f49e6739510c1d7dbb1fab6de65a5e8f8345a3cff103d6ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_5d84369d6a309540f49e6739510c1d7dbb1fab6de65a5e8f8345a3cff103d6ce->leave($__internal_5d84369d6a309540f49e6739510c1d7dbb1fab6de65a5e8f8345a3cff103d6ce_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
