<?php

/* form/activate_user.html.twig */
class __TwigTemplate_bcc0f81768aa86cc65f72fb6402b42ed34fac2088ff3ec7d6dbcb4c81332e7c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c879b6d919eb6171093109bc300fe9db3ab624b0e80e7a716dc9db1beec485c2 = $this->env->getExtension("native_profiler");
        $__internal_c879b6d919eb6171093109bc300fe9db3ab624b0e80e7a716dc9db1beec485c2->enter($__internal_c879b6d919eb6171093109bc300fe9db3ab624b0e80e7a716dc9db1beec485c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/activate_user.html.twig"));

        // line 1
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 2
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), 'errors');
        echo "
";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), 'widget');
        echo "
";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), 'widget');
        echo "
";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        
        $__internal_c879b6d919eb6171093109bc300fe9db3ab624b0e80e7a716dc9db1beec485c2->leave($__internal_c879b6d919eb6171093109bc300fe9db3ab624b0e80e7a716dc9db1beec485c2_prof);

    }

    public function getTemplateName()
    {
        return "form/activate_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 5,  34 => 4,  30 => 3,  26 => 2,  22 => 1,);
    }
}
/* {{ form_start(form) }}*/
/* {{ form_errors(form.plainPassword) }}*/
/* {{ form_widget(form.plainPassword) }}*/
/* {{ form_widget(form.plainPassword) }}*/
/* {{ form_end(form) }}*/
