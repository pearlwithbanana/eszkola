<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_452c6b667ac2249dabd7609c231c44401c8c062f66178dab784bfc5e808564b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46ffd1f54d26aadba281b4aaa1034a66790d21487731b4f87d766fe9b601151e = $this->env->getExtension("native_profiler");
        $__internal_46ffd1f54d26aadba281b4aaa1034a66790d21487731b4f87d766fe9b601151e->enter($__internal_46ffd1f54d26aadba281b4aaa1034a66790d21487731b4f87d766fe9b601151e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_46ffd1f54d26aadba281b4aaa1034a66790d21487731b4f87d766fe9b601151e->leave($__internal_46ffd1f54d26aadba281b4aaa1034a66790d21487731b4f87d766fe9b601151e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
