<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_eaf21adcb4b7d1baee184759f69c719e170bdbe8f6edf1f8acd7a268cc8d1d29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f397dec5219da1ddf4b31e307896b5c0b33dd57c0fb924ea2424a739764d1bc3 = $this->env->getExtension("native_profiler");
        $__internal_f397dec5219da1ddf4b31e307896b5c0b33dd57c0fb924ea2424a739764d1bc3->enter($__internal_f397dec5219da1ddf4b31e307896b5c0b33dd57c0fb924ea2424a739764d1bc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_f397dec5219da1ddf4b31e307896b5c0b33dd57c0fb924ea2424a739764d1bc3->leave($__internal_f397dec5219da1ddf4b31e307896b5c0b33dd57c0fb924ea2424a739764d1bc3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
