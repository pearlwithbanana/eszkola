<?php

/* form/sys_conf/first_step.html.twig */
class __TwigTemplate_25838d82d43ac3a6ce2b9b2320282724e6a1e5afc51d94c24fa6ad1c4a397d9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39cfe958a98310f9b0069b4ed5c83ecc53f8118f6fbffa1f6700951d74c7cc90 = $this->env->getExtension("native_profiler");
        $__internal_39cfe958a98310f9b0069b4ed5c83ecc53f8118f6fbffa1f6700951d74c7cc90->enter($__internal_39cfe958a98310f9b0069b4ed5c83ecc53f8118f6fbffa1f6700951d74c7cc90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/sys_conf/first_step.html.twig"));

        // line 1
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
";
        // line 2
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "divisions", array()), 'label');
        echo "
<ul id=\"fields-list\"
    data-prototype='
    ";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "divisions", array()), "vars", array()), "prototype", array()), 'widget'));
        echo "
    '>
    <button type=\"button\" id=\"add-another-input\">Dodaj pole</button>
    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "divisions", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["divisionField"]) {
            // line 10
            echo "        <li>
            ";
            // line 11
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["divisionField"], 'errors');
            echo "
            ";
            // line 12
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["divisionField"], 'widget');
            echo "
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['divisionField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "</ul>
";
        // line 16
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/js/add-input.js"), "html", null, true);
        echo "\"></script>


";
        
        $__internal_39cfe958a98310f9b0069b4ed5c83ecc53f8118f6fbffa1f6700951d74c7cc90->leave($__internal_39cfe958a98310f9b0069b4ed5c83ecc53f8118f6fbffa1f6700951d74c7cc90_prof);

    }

    public function getTemplateName()
    {
        return "form/sys_conf/first_step.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 18,  65 => 16,  62 => 15,  53 => 12,  49 => 11,  46 => 10,  42 => 9,  36 => 6,  30 => 3,  26 => 2,  22 => 1,);
    }
}
/* {{ form_errors(form) }}*/
/* {{ form_start(form) }}*/
/* {{ form_label(form.divisions) }}*/
/* <ul id="fields-list"*/
/*     data-prototype='*/
/*     {{ form_widget(form.divisions.vars.prototype)|e }}*/
/*     '>*/
/*     <button type="button" id="add-another-input">Dodaj pole</button>*/
/*     {% for divisionField in form.divisions %}*/
/*         <li>*/
/*             {{ form_errors(divisionField) }}*/
/*             {{ form_widget(divisionField) }}*/
/*         </li>*/
/*     {% endfor %}*/
/* </ul>*/
/* {{ form_end(form) }}*/
/* <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>*/
/* <script src="{{ asset('assets/js/add-input.js') }}"></script>*/
/* */
/* */
/* */
