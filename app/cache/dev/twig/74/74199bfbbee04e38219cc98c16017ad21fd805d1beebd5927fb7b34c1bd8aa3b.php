<?php

/* message/link_expired.twig */
class __TwigTemplate_6279597d673ff4d5f65e424183055336bf444d94cef14569ac68ecb6a234c663 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8cf15104383d9e8eb1f206339d24861edb64821a4b0bc59df8e1bf9879a91af6 = $this->env->getExtension("native_profiler");
        $__internal_8cf15104383d9e8eb1f206339d24861edb64821a4b0bc59df8e1bf9879a91af6->enter($__internal_8cf15104383d9e8eb1f206339d24861edb64821a4b0bc59df8e1bf9879a91af6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message/link_expired.twig"));

        // line 1
        echo "<h1>Link aktywacyjny wygasł lub jest nieprawidłowy. Proszę skontaktować się z administracją. </h1>
<p>Uprzejmie przypominamy, że czas aktywacji konta wynosi 14 dni. </p>";
        
        $__internal_8cf15104383d9e8eb1f206339d24861edb64821a4b0bc59df8e1bf9879a91af6->leave($__internal_8cf15104383d9e8eb1f206339d24861edb64821a4b0bc59df8e1bf9879a91af6_prof);

    }

    public function getTemplateName()
    {
        return "message/link_expired.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <h1>Link aktywacyjny wygasł lub jest nieprawidłowy. Proszę skontaktować się z administracją. </h1>*/
/* <p>Uprzejmie przypominamy, że czas aktywacji konta wynosi 14 dni. </p>*/
