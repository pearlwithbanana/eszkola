<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_1bb9f40d63cc2528d0f3fc95e7b885a48833b4c8e2dc4e752eec52808c906570 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_106873d260553b897d77f3f48e1028a38e4133cba680a43618e230aab64ea78b = $this->env->getExtension("native_profiler");
        $__internal_106873d260553b897d77f3f48e1028a38e4133cba680a43618e230aab64ea78b->enter($__internal_106873d260553b897d77f3f48e1028a38e4133cba680a43618e230aab64ea78b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_106873d260553b897d77f3f48e1028a38e4133cba680a43618e230aab64ea78b->leave($__internal_106873d260553b897d77f3f48e1028a38e4133cba680a43618e230aab64ea78b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
