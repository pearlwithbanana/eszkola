<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_37ffc358396c7af152f99d5dc802c7686e4a9fb1f11c1027fadb995ce03e5e3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac402c8e9c738209a59967ce33232c0bdafafb7d272d329d1690b937f0ba94ff = $this->env->getExtension("native_profiler");
        $__internal_ac402c8e9c738209a59967ce33232c0bdafafb7d272d329d1690b937f0ba94ff->enter($__internal_ac402c8e9c738209a59967ce33232c0bdafafb7d272d329d1690b937f0ba94ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_ac402c8e9c738209a59967ce33232c0bdafafb7d272d329d1690b937f0ba94ff->leave($__internal_ac402c8e9c738209a59967ce33232c0bdafafb7d272d329d1690b937f0ba94ff_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
