<?php

/* base.html.twig */
class __TwigTemplate_e1914c3cf221cc824ab05548fb764eb2f597a0fbc37fe5df24d0f90482e9c6db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheet' => array($this, 'block_stylesheet'),
            'container' => array($this, 'block_container'),
            'footer' => array($this, 'block_footer'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a9808974afc0b96de154806f7de3bc3083174c3fe1969f369cceeb957932045 = $this->env->getExtension("native_profiler");
        $__internal_4a9808974afc0b96de154806f7de3bc3083174c3fe1969f369cceeb957932045->enter($__internal_4a9808974afc0b96de154806f7de3bc3083174c3fe1969f369cceeb957932045_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        $this->displayBlock('header', $context, $blocks);
        // line 8
        echo "<body>
<div class=\"container col-md-12\">
    ";
        // line 10
        $this->displayBlock('container', $context, $blocks);
        // line 13
        echo "</div>
</body>
";
        // line 15
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_4a9808974afc0b96de154806f7de3bc3083174c3fe1969f369cceeb957932045->leave($__internal_4a9808974afc0b96de154806f7de3bc3083174c3fe1969f369cceeb957932045_prof);

    }

    // line 1
    public function block_header($context, array $blocks = array())
    {
        $__internal_25ef2ad2f8bc51f25a014bc8bf9fc2504a73d703b9e57ee61defb44c54e28952 = $this->env->getExtension("native_profiler");
        $__internal_25ef2ad2f8bc51f25a014bc8bf9fc2504a73d703b9e57ee61defb44c54e28952->enter($__internal_25ef2ad2f8bc51f25a014bc8bf9fc2504a73d703b9e57ee61defb44c54e28952_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 2
        echo "    <meta charset=\"utf-8\" lang=\"pl\"/>
    <tittle>Elearning - twoja platforma szkoły</tittle>
    ";
        // line 4
        $this->displayBlock('stylesheet', $context, $blocks);
        
        $__internal_25ef2ad2f8bc51f25a014bc8bf9fc2504a73d703b9e57ee61defb44c54e28952->leave($__internal_25ef2ad2f8bc51f25a014bc8bf9fc2504a73d703b9e57ee61defb44c54e28952_prof);

    }

    public function block_stylesheet($context, array $blocks = array())
    {
        $__internal_24854edf78f3fa728866f5ad0163c0432d5d78a1d207c5c9c5f28c2efd04e273 = $this->env->getExtension("native_profiler");
        $__internal_24854edf78f3fa728866f5ad0163c0432d5d78a1d207c5c9c5f28c2efd04e273->enter($__internal_24854edf78f3fa728866f5ad0163c0432d5d78a1d207c5c9c5f28c2efd04e273_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheet"));

        // line 5
        echo "        <style rel=\"stylesheet\" type=\"text/css\"  href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" ></style>
    ";
        
        $__internal_24854edf78f3fa728866f5ad0163c0432d5d78a1d207c5c9c5f28c2efd04e273->leave($__internal_24854edf78f3fa728866f5ad0163c0432d5d78a1d207c5c9c5f28c2efd04e273_prof);

    }

    // line 10
    public function block_container($context, array $blocks = array())
    {
        $__internal_46d2b9ad5ae06f791b9ae9ff7e732f41a5cab559407e6592317ba9caf07776b8 = $this->env->getExtension("native_profiler");
        $__internal_46d2b9ad5ae06f791b9ae9ff7e732f41a5cab559407e6592317ba9caf07776b8->enter($__internal_46d2b9ad5ae06f791b9ae9ff7e732f41a5cab559407e6592317ba9caf07776b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 11
        echo "
    ";
        
        $__internal_46d2b9ad5ae06f791b9ae9ff7e732f41a5cab559407e6592317ba9caf07776b8->leave($__internal_46d2b9ad5ae06f791b9ae9ff7e732f41a5cab559407e6592317ba9caf07776b8_prof);

    }

    // line 15
    public function block_footer($context, array $blocks = array())
    {
        $__internal_a83b183f34f8bab15e2204b5ad1a26569c8a7c7acd5c5a5b4ca4c416921be949 = $this->env->getExtension("native_profiler");
        $__internal_a83b183f34f8bab15e2204b5ad1a26569c8a7c7acd5c5a5b4ca4c416921be949->enter($__internal_a83b183f34f8bab15e2204b5ad1a26569c8a7c7acd5c5a5b4ca4c416921be949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 16
        echo "    ";
        $this->displayBlock('script', $context, $blocks);
        
        $__internal_a83b183f34f8bab15e2204b5ad1a26569c8a7c7acd5c5a5b4ca4c416921be949->leave($__internal_a83b183f34f8bab15e2204b5ad1a26569c8a7c7acd5c5a5b4ca4c416921be949_prof);

    }

    public function block_script($context, array $blocks = array())
    {
        $__internal_0a319dff0c470f719b662523f02501908caf112dfd149ba73547da5cc33c7807 = $this->env->getExtension("native_profiler");
        $__internal_0a319dff0c470f719b662523f02501908caf112dfd149ba73547da5cc33c7807->enter($__internal_0a319dff0c470f719b662523f02501908caf112dfd149ba73547da5cc33c7807_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 17
        echo "    ";
        
        $__internal_0a319dff0c470f719b662523f02501908caf112dfd149ba73547da5cc33c7807->leave($__internal_0a319dff0c470f719b662523f02501908caf112dfd149ba73547da5cc33c7807_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  109 => 17,  96 => 16,  90 => 15,  82 => 11,  76 => 10,  68 => 5,  56 => 4,  52 => 2,  46 => 1,  39 => 15,  35 => 13,  33 => 10,  29 => 8,  27 => 1,);
    }
}
/* {% block header %}*/
/*     <meta charset="utf-8" lang="pl"/>*/
/*     <tittle>Elearning - twoja platforma szkoły</tittle>*/
/*     {% block stylesheet %}*/
/*         <style rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" ></style>*/
/*     {% endblock %}*/
/* {% endblock %}*/
/* <body>*/
/* <div class="container col-md-12">*/
/*     {% block container %}*/
/* */
/*     {% endblock %}*/
/* </div>*/
/* </body>*/
/* {% block footer %}*/
/*     {% block script %}*/
/*     {% endblock %}*/
/* {% endblock %}*/
