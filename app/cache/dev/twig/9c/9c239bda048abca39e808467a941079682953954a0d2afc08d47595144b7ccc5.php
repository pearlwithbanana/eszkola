<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_fa07f3320578abcdb220307b3b6e03e8e0e3b46c8253a2fdeb95a393f17f5a84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77f6e3069e3de015982d5dba1d7860ca92a231bc0affcac74f47d22ae92ba585 = $this->env->getExtension("native_profiler");
        $__internal_77f6e3069e3de015982d5dba1d7860ca92a231bc0affcac74f47d22ae92ba585->enter($__internal_77f6e3069e3de015982d5dba1d7860ca92a231bc0affcac74f47d22ae92ba585_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_77f6e3069e3de015982d5dba1d7860ca92a231bc0affcac74f47d22ae92ba585->leave($__internal_77f6e3069e3de015982d5dba1d7860ca92a231bc0affcac74f47d22ae92ba585_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
