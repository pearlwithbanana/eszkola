<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_422c1b6bbb4cd5b460d367a208b457fa808109d1ced50fdfad97c67184cd66a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_577d9c9983f9fce2847b643bfbede5009f1a47c195ade482eadee7f40c26655b = $this->env->getExtension("native_profiler");
        $__internal_577d9c9983f9fce2847b643bfbede5009f1a47c195ade482eadee7f40c26655b->enter($__internal_577d9c9983f9fce2847b643bfbede5009f1a47c195ade482eadee7f40c26655b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_577d9c9983f9fce2847b643bfbede5009f1a47c195ade482eadee7f40c26655b->leave($__internal_577d9c9983f9fce2847b643bfbede5009f1a47c195ade482eadee7f40c26655b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
