<?php

/* :form:register.html.twig */
class __TwigTemplate_ae01f41f4b6e211d1b33b4d7caf2c29e8589d56ea1ceeb680e0bcdfec517ff98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_745ca9859a8af7162740a20ef50112105dff8097aa702addda41e680c8dcc1cf = $this->env->getExtension("native_profiler");
        $__internal_745ca9859a8af7162740a20ef50112105dff8097aa702addda41e680c8dcc1cf->enter($__internal_745ca9859a8af7162740a20ef50112105dff8097aa702addda41e680c8dcc1cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":form:register.html.twig"));

        // line 1
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
";
        // line 2
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'row');
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lastName", array()), 'row');
        echo "
    ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'row');
        echo "
";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

";
        
        $__internal_745ca9859a8af7162740a20ef50112105dff8097aa702addda41e680c8dcc1cf->leave($__internal_745ca9859a8af7162740a20ef50112105dff8097aa702addda41e680c8dcc1cf_prof);

    }

    public function getTemplateName()
    {
        return ":form:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 6,  38 => 5,  34 => 4,  30 => 3,  26 => 2,  22 => 1,);
    }
}
/* {{ form_errors(form) }}*/
/* {{ form_start(form) }}*/
/*     {{ form_row(form.name) }}*/
/*     {{ form_row(form.lastName) }}*/
/*     {{ form_row(form.email) }}*/
/* {{ form_end(form) }}*/
/* */
/* */
