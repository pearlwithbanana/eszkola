<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_d40236ae070f88b22c94e245f55f4cb2562d80e9d39d9152e7a8a4e12e316065 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3e9440949c763e5fd39943e1e252d3daf9b37cb90fb825ba60f61ddb5698ee2 = $this->env->getExtension("native_profiler");
        $__internal_e3e9440949c763e5fd39943e1e252d3daf9b37cb90fb825ba60f61ddb5698ee2->enter($__internal_e3e9440949c763e5fd39943e1e252d3daf9b37cb90fb825ba60f61ddb5698ee2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_e3e9440949c763e5fd39943e1e252d3daf9b37cb90fb825ba60f61ddb5698ee2->leave($__internal_e3e9440949c763e5fd39943e1e252d3daf9b37cb90fb825ba60f61ddb5698ee2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
