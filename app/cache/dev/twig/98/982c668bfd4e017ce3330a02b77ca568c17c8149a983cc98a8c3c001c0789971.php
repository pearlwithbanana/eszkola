<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_87dd86a3df8b608f36ed970f14c522e5b9f3071d3c9987fae84fe4c64d9c030d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28ebed71948396d0f2d1b4a40e54a624bbb83bfee447b93905f28484204dc9b9 = $this->env->getExtension("native_profiler");
        $__internal_28ebed71948396d0f2d1b4a40e54a624bbb83bfee447b93905f28484204dc9b9->enter($__internal_28ebed71948396d0f2d1b4a40e54a624bbb83bfee447b93905f28484204dc9b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_28ebed71948396d0f2d1b4a40e54a624bbb83bfee447b93905f28484204dc9b9->leave($__internal_28ebed71948396d0f2d1b4a40e54a624bbb83bfee447b93905f28484204dc9b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
