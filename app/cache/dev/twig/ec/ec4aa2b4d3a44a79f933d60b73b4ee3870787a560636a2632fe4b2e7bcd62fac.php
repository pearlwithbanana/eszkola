<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e40cbe33847fc0d1253518e1aa8404f2afd89022bbe58cd364ca88b015f01b40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1cf0d9f28001f0f073e976b81df1cd742c2784db000b5926bfcf35408b293337 = $this->env->getExtension("native_profiler");
        $__internal_1cf0d9f28001f0f073e976b81df1cd742c2784db000b5926bfcf35408b293337->enter($__internal_1cf0d9f28001f0f073e976b81df1cd742c2784db000b5926bfcf35408b293337_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1cf0d9f28001f0f073e976b81df1cd742c2784db000b5926bfcf35408b293337->leave($__internal_1cf0d9f28001f0f073e976b81df1cd742c2784db000b5926bfcf35408b293337_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_08a5405edb804a36f67be6df393f93c427f1b6a6e4d0bb1aa488ff14ff55e98e = $this->env->getExtension("native_profiler");
        $__internal_08a5405edb804a36f67be6df393f93c427f1b6a6e4d0bb1aa488ff14ff55e98e->enter($__internal_08a5405edb804a36f67be6df393f93c427f1b6a6e4d0bb1aa488ff14ff55e98e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_08a5405edb804a36f67be6df393f93c427f1b6a6e4d0bb1aa488ff14ff55e98e->leave($__internal_08a5405edb804a36f67be6df393f93c427f1b6a6e4d0bb1aa488ff14ff55e98e_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_50c532c4ddbe3d2a04564d6d2f6153c329251b0056c9af2185c81c8079fc10bc = $this->env->getExtension("native_profiler");
        $__internal_50c532c4ddbe3d2a04564d6d2f6153c329251b0056c9af2185c81c8079fc10bc->enter($__internal_50c532c4ddbe3d2a04564d6d2f6153c329251b0056c9af2185c81c8079fc10bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_50c532c4ddbe3d2a04564d6d2f6153c329251b0056c9af2185c81c8079fc10bc->leave($__internal_50c532c4ddbe3d2a04564d6d2f6153c329251b0056c9af2185c81c8079fc10bc_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_9d7142f87405cd465a217cdcc469c7590bffea690ceaa6bc269ca7fa916555c8 = $this->env->getExtension("native_profiler");
        $__internal_9d7142f87405cd465a217cdcc469c7590bffea690ceaa6bc269ca7fa916555c8->enter($__internal_9d7142f87405cd465a217cdcc469c7590bffea690ceaa6bc269ca7fa916555c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_9d7142f87405cd465a217cdcc469c7590bffea690ceaa6bc269ca7fa916555c8->leave($__internal_9d7142f87405cd465a217cdcc469c7590bffea690ceaa6bc269ca7fa916555c8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
