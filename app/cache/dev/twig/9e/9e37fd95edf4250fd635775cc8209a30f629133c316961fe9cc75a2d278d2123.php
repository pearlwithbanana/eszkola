<?php

/* form/add_user */
class __TwigTemplate_6e13fbdafe0b679a1ea23d4e0fb2927fc6d1cc501dd32f6ea12f38e1a675799a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'user_form' => array($this, 'block_user_form'),
            'prototypes' => array($this, 'block_prototypes'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_449d8598e8aec9f3748992bab56ffb0723284099e89d2ebca3a148fd67ed5b1a = $this->env->getExtension("native_profiler");
        $__internal_449d8598e8aec9f3748992bab56ffb0723284099e89d2ebca3a148fd67ed5b1a->enter($__internal_449d8598e8aec9f3748992bab56ffb0723284099e89d2ebca3a148fd67ed5b1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/add_user"));

        // line 1
        $this->displayBlock('user_form', $context, $blocks);
        // line 15
        echo "


";
        // line 18
        $this->displayBlock('javascripts', $context, $blocks);
        
        $__internal_449d8598e8aec9f3748992bab56ffb0723284099e89d2ebca3a148fd67ed5b1a->leave($__internal_449d8598e8aec9f3748992bab56ffb0723284099e89d2ebca3a148fd67ed5b1a_prof);

    }

    // line 1
    public function block_user_form($context, array $blocks = array())
    {
        $__internal_556ad7f3f72c434c0d2fd3b72ff1daff424ed69b2041e6ac99ff0553dc11a11c = $this->env->getExtension("native_profiler");
        $__internal_556ad7f3f72c434c0d2fd3b72ff1daff424ed69b2041e6ac99ff0553dc11a11c->enter($__internal_556ad7f3f72c434c0d2fd3b72ff1daff424ed69b2041e6ac99ff0553dc11a11c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_form"));

        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "users", array()), 'errors');
        echo "
    ";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    <ul id=\"fields-list\" data-prototype='
    ";
        // line 5
        $this->displayBlock('prototypes', $context, $blocks);
        // line 10
        echo "    '>
    </ul>
    ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 13
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_556ad7f3f72c434c0d2fd3b72ff1daff424ed69b2041e6ac99ff0553dc11a11c->leave($__internal_556ad7f3f72c434c0d2fd3b72ff1daff424ed69b2041e6ac99ff0553dc11a11c_prof);

    }

    // line 5
    public function block_prototypes($context, array $blocks = array())
    {
        $__internal_4f0443023b3770bd0f08c19b3f31b4a53187b585e0d9483979714caa2d6cc8c7 = $this->env->getExtension("native_profiler");
        $__internal_4f0443023b3770bd0f08c19b3f31b4a53187b585e0d9483979714caa2d6cc8c7->enter($__internal_4f0443023b3770bd0f08c19b3f31b4a53187b585e0d9483979714caa2d6cc8c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "prototypes"));

        // line 6
        echo "    ";
        // line 7
        echo "    ";
        // line 8
        echo "    ";
        // line 9
        echo "    ";
        
        $__internal_4f0443023b3770bd0f08c19b3f31b4a53187b585e0d9483979714caa2d6cc8c7->leave($__internal_4f0443023b3770bd0f08c19b3f31b4a53187b585e0d9483979714caa2d6cc8c7_prof);

    }

    // line 18
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9d9e7c86646d38ef586530c0861c302c96e5b73e096681fbea51757fb3e6e027 = $this->env->getExtension("native_profiler");
        $__internal_9d9e7c86646d38ef586530c0861c302c96e5b73e096681fbea51757fb3e6e027->enter($__internal_9d9e7c86646d38ef586530c0861c302c96e5b73e096681fbea51757fb3e6e027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 19
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/js/add-input.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_9d9e7c86646d38ef586530c0861c302c96e5b73e096681fbea51757fb3e6e027->leave($__internal_9d9e7c86646d38ef586530c0861c302c96e5b73e096681fbea51757fb3e6e027_prof);

    }

    public function getTemplateName()
    {
        return "form/add_user";
    }

    public function getDebugInfo()
    {
        return array (  102 => 20,  99 => 19,  93 => 18,  86 => 9,  84 => 8,  82 => 7,  80 => 6,  74 => 5,  65 => 13,  61 => 12,  57 => 10,  55 => 5,  50 => 3,  45 => 2,  39 => 1,  32 => 18,  27 => 15,  25 => 1,);
    }
}
/* {% block user_form %}*/
/*     {{ form_errors(form.users) }}*/
/*     {{ form_start(form) }}*/
/*     <ul id="fields-list" data-prototype='*/
/*     {% block prototypes %}*/
/*     {#{{ form_widget(form.users.nameSurnameCollection.vars.prototype)|e }}#}*/
/*     {#{{ form_widget(form.users.emailCollection.vars.prototype|e) }}#}*/
/*     {#{{ form_widget(form.users.dateOfBirthCollection.vars.prototype|e) }}#}*/
/*     {% endblock %}*/
/*     '>*/
/*     </ul>*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/* {% endblock %}*/
/* */
/* */
/* */
/* {% block javascripts %}*/
/*     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>*/
/*     <script src="{{ asset('assets/js/add-input.js') }}"></script>*/
/* {% endblock %}*/
