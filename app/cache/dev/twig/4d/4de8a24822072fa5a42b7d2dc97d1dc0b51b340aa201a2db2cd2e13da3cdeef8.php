<?php

/* form/add_teacher */
class __TwigTemplate_a436bca97844b870650cead105536b8654338c520e92f7d9887df999de89d29b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4bed8bc12a2f285f855c6bd306630a7e10516cceff8944d4e6639d19e87e753 = $this->env->getExtension("native_profiler");
        $__internal_e4bed8bc12a2f285f855c6bd306630a7e10516cceff8944d4e6639d19e87e753->enter($__internal_e4bed8bc12a2f285f855c6bd306630a7e10516cceff8944d4e6639d19e87e753_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/add_teacher"));

        // line 1
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
<button id=\"add_field\">Dodaj pole</button>
<ul id=\"input_collection\" data-prototype='
";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "users", array()), "vars", array()), "prototype", array()), 'widget'));
        echo "
<button data-field-button=\"remove\" type=\"button\">Usuń</button>
'>
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "users", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 8
            echo "        <li>
            ";
            // line 9
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($context["user"], "fullName", array()), 'row');
            echo "
            ";
            // line 10
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($context["user"], "email", array()), 'row');
            echo "
            ";
            // line 11
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($context["user"], "phoneNumber", array()), 'row');
            echo "
            ";
            // line 12
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($context["user"], "dateOfBirth", array()), 'row');
            echo "
            ";
            // line 13
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            echo "
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "</ul>
";
        // line 17
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
<script>
    var collectionHolder;
    var addField = \$('#add_field');

    \$(document).ready(function() {
        collectionHolder = \$('#input_collection');
        collectionHolder.data('index', collectionHolder.find(':input').length);
        addField.on('click', function(event) {
            event.preventDefault();
            addFieldForm(collectionHolder);
        });
    });

    function updateIndex(collectionHolder, value) {
        var prototype = collectionHolder.data('prototype');
        var newForm = prototype.replace(/__name__/g, index);
        var index = collectionHolder.data('index');
        collectionHolder.data('index', index + value);
        return newForm;
    }

    function addFieldForm(collectionHolder) {
        var prototype = collectionHolder.data('prototype');
        var index = collectionHolder.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        collectionHolder.data('index', index + 1);
        var newFormLi = \$('<li></li>').append(newForm);
        newFormLi.appendTo(collectionHolder);
        new removeButtonSwitch(collectionHolder);
    }

    function removeButtonSwitch(collectionHolder) {
        updateIndex(collectionHolder, -1);
        \$('[data-field-button=\"remove\"]').click(function() {
            \$(this).closest('li').remove();
        });
    }



</script>";
        
        $__internal_e4bed8bc12a2f285f855c6bd306630a7e10516cceff8944d4e6639d19e87e753->leave($__internal_e4bed8bc12a2f285f855c6bd306630a7e10516cceff8944d4e6639d19e87e753_prof);

    }

    public function getTemplateName()
    {
        return "form/add_teacher";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 17,  66 => 16,  57 => 13,  53 => 12,  49 => 11,  45 => 10,  41 => 9,  38 => 8,  34 => 7,  28 => 4,  22 => 1,);
    }
}
/* {{ form_start(form) }}*/
/* <button id="add_field">Dodaj pole</button>*/
/* <ul id="input_collection" data-prototype='*/
/* {{ form_widget(form.users.vars.prototype)|e }}*/
/* <button data-field-button="remove" type="button">Usuń</button>*/
/* '>*/
/*     {%  for user in form.users %}*/
/*         <li>*/
/*             {{ form_row(user.fullName) }}*/
/*             {{ form_row(user.email) }}*/
/*             {{ form_row(user.phoneNumber) }}*/
/*             {{ form_row(user.dateOfBirth) }}*/
/*             {{ form_errors(form) }}*/
/*         </li>*/
/*     {% endfor %}*/
/* </ul>*/
/* {{ form_end(form) }}*/
/* */
/* <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>*/
/* <script>*/
/*     var collectionHolder;*/
/*     var addField = $('#add_field');*/
/* */
/*     $(document).ready(function() {*/
/*         collectionHolder = $('#input_collection');*/
/*         collectionHolder.data('index', collectionHolder.find(':input').length);*/
/*         addField.on('click', function(event) {*/
/*             event.preventDefault();*/
/*             addFieldForm(collectionHolder);*/
/*         });*/
/*     });*/
/* */
/*     function updateIndex(collectionHolder, value) {*/
/*         var prototype = collectionHolder.data('prototype');*/
/*         var newForm = prototype.replace(/__name__/g, index);*/
/*         var index = collectionHolder.data('index');*/
/*         collectionHolder.data('index', index + value);*/
/*         return newForm;*/
/*     }*/
/* */
/*     function addFieldForm(collectionHolder) {*/
/*         var prototype = collectionHolder.data('prototype');*/
/*         var index = collectionHolder.data('index');*/
/*         var newForm = prototype.replace(/__name__/g, index);*/
/*         collectionHolder.data('index', index + 1);*/
/*         var newFormLi = $('<li></li>').append(newForm);*/
/*         newFormLi.appendTo(collectionHolder);*/
/*         new removeButtonSwitch(collectionHolder);*/
/*     }*/
/* */
/*     function removeButtonSwitch(collectionHolder) {*/
/*         updateIndex(collectionHolder, -1);*/
/*         $('[data-field-button="remove"]').click(function() {*/
/*             $(this).closest('li').remove();*/
/*         });*/
/*     }*/
/* */
/* */
/* */
/* </script>*/
