<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_f64e346f2ec325289b9062cc40a25a6197e54dfec3f3da13c1a3a06e8ed6e142 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9a4f782e8fefc86088e295cefa581a6c3f05acd1f496132f18b8f2198d9b6b5 = $this->env->getExtension("native_profiler");
        $__internal_c9a4f782e8fefc86088e295cefa581a6c3f05acd1f496132f18b8f2198d9b6b5->enter($__internal_c9a4f782e8fefc86088e295cefa581a6c3f05acd1f496132f18b8f2198d9b6b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_c9a4f782e8fefc86088e295cefa581a6c3f05acd1f496132f18b8f2198d9b6b5->leave($__internal_c9a4f782e8fefc86088e295cefa581a6c3f05acd1f496132f18b8f2198d9b6b5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
