<?php

/* @WebProfiler/Icon/no.svg */
class __TwigTemplate_6341ee5f1c05ce64cb7836c2f1a3c898fb28dd8c0864e9de3e5933727a41220e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c44b1cc2d03a79da9b53a10e762498de5c3bd387d6cf7ab850ac758018f7e7d5 = $this->env->getExtension("native_profiler");
        $__internal_c44b1cc2d03a79da9b53a10e762498de5c3bd387d6cf7ab850ac758018f7e7d5->enter($__internal_c44b1cc2d03a79da9b53a10e762498de5c3bd387d6cf7ab850ac758018f7e7d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
";
        
        $__internal_c44b1cc2d03a79da9b53a10e762498de5c3bd387d6cf7ab850ac758018f7e7d5->leave($__internal_c44b1cc2d03a79da9b53a10e762498de5c3bd387d6cf7ab850ac758018f7e7d5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/no.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="28" viewBox="0 0 12 12" enable-background="new 0 0 12 12" xml:space="preserve">*/
/*     <path fill="#B0413E" d="M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4*/
/*     C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2*/
/*     C11.1,10,11.2,9.2,10.4,8.4z"/>*/
/* </svg>*/
/* */
