<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_c3bcf74527192a67819c3af42786e96e69f42a67ef4fdc49cf2d4652c5338c7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8afb1d5d6f18ce6a41568f3cb4d87978605e4a8e3295b59c861ca87b6732150 = $this->env->getExtension("native_profiler");
        $__internal_d8afb1d5d6f18ce6a41568f3cb4d87978605e4a8e3295b59c861ca87b6732150->enter($__internal_d8afb1d5d6f18ce6a41568f3cb4d87978605e4a8e3295b59c861ca87b6732150_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_d8afb1d5d6f18ce6a41568f3cb4d87978605e4a8e3295b59c861ca87b6732150->leave($__internal_d8afb1d5d6f18ce6a41568f3cb4d87978605e4a8e3295b59c861ca87b6732150_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
