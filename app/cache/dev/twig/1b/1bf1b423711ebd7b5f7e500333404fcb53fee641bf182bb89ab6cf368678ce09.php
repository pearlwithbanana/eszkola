<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_cbe80466ed1507e9dc28467afee151ddff324f0a793f92ff3badf8435d37299e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7533ab2626684c817955a0836f7fb4884494bb3d9959ec1b623d228774e14b6 = $this->env->getExtension("native_profiler");
        $__internal_d7533ab2626684c817955a0836f7fb4884494bb3d9959ec1b623d228774e14b6->enter($__internal_d7533ab2626684c817955a0836f7fb4884494bb3d9959ec1b623d228774e14b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_d7533ab2626684c817955a0836f7fb4884494bb3d9959ec1b623d228774e14b6->leave($__internal_d7533ab2626684c817955a0836f7fb4884494bb3d9959ec1b623d228774e14b6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
