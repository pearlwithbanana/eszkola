<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_a7370de931ef43aea424d4f49106346cdc95d1b053a7693ba01fb54d08ed0b29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5161bbeb17b3b29f7f418feb47f3b6546d943e15f27e67f4fda0fa9751fa09e6 = $this->env->getExtension("native_profiler");
        $__internal_5161bbeb17b3b29f7f418feb47f3b6546d943e15f27e67f4fda0fa9751fa09e6->enter($__internal_5161bbeb17b3b29f7f418feb47f3b6546d943e15f27e67f4fda0fa9751fa09e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_5161bbeb17b3b29f7f418feb47f3b6546d943e15f27e67f4fda0fa9751fa09e6->leave($__internal_5161bbeb17b3b29f7f418feb47f3b6546d943e15f27e67f4fda0fa9751fa09e6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
