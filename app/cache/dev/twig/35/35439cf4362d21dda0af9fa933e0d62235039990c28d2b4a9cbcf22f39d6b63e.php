<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_b8b4ef6f3b7a2fbe591ebfc82cba64f6bbcaaae0ba27653c9bafee1dd124e206 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77269e8277ec1301935ca6b747c93c7e78205820eeaa92fcd084a7be8d948277 = $this->env->getExtension("native_profiler");
        $__internal_77269e8277ec1301935ca6b747c93c7e78205820eeaa92fcd084a7be8d948277->enter($__internal_77269e8277ec1301935ca6b747c93c7e78205820eeaa92fcd084a7be8d948277_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_77269e8277ec1301935ca6b747c93c7e78205820eeaa92fcd084a7be8d948277->leave($__internal_77269e8277ec1301935ca6b747c93c7e78205820eeaa92fcd084a7be8d948277_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
