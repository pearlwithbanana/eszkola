<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_44b01e112d71f1e60729c1044815e5e6c52421cc537da462ac0af87138c9a34c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86ae384e82b339798fbb00ced2d49fe23fa725fb3708bbfe9476a06f8f65175d = $this->env->getExtension("native_profiler");
        $__internal_86ae384e82b339798fbb00ced2d49fe23fa725fb3708bbfe9476a06f8f65175d->enter($__internal_86ae384e82b339798fbb00ced2d49fe23fa725fb3708bbfe9476a06f8f65175d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_86ae384e82b339798fbb00ced2d49fe23fa725fb3708bbfe9476a06f8f65175d->leave($__internal_86ae384e82b339798fbb00ced2d49fe23fa725fb3708bbfe9476a06f8f65175d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
