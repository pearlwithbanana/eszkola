<?php

/* :form:student.html.twig */
class __TwigTemplate_d5005296ca8ff0b6fdbf8a643f523de49b49f93e23a5d0a1d0a67b4d0ee29d57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f969b73a760330b0655c8e3046f4f062ce01357851168cb2df56c5c95d83d5bb = $this->env->getExtension("native_profiler");
        $__internal_f969b73a760330b0655c8e3046f4f062ce01357851168cb2df56c5c95d83d5bb->enter($__internal_f969b73a760330b0655c8e3046f4f062ce01357851168cb2df56c5c95d83d5bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":form:student.html.twig"));

        // line 1
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
";
        // line 2
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "studentDivision", array()), 'row');
        echo "
";
        // line 4
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        
        $__internal_f969b73a760330b0655c8e3046f4f062ce01357851168cb2df56c5c95d83d5bb->leave($__internal_f969b73a760330b0655c8e3046f4f062ce01357851168cb2df56c5c95d83d5bb_prof);

    }

    public function getTemplateName()
    {
        return ":form:student.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 4,  30 => 3,  26 => 2,  22 => 1,);
    }
}
/* {{ form_errors(form) }}*/
/* {{ form_start(form) }}*/
/* {{ form_row(form.studentDivision) }}*/
/* {{ form_end(form) }}*/
