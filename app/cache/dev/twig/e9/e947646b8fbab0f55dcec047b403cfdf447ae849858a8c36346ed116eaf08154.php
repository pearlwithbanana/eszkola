<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_9e51fa8001747748d09dfa79321adc138fa2d0b1274f0cf0f4e3a792aff25000 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e22ce1dd822dd09cd9ace9517dcfcaaaf47752dcffa20784219c5aec0c79dac6 = $this->env->getExtension("native_profiler");
        $__internal_e22ce1dd822dd09cd9ace9517dcfcaaaf47752dcffa20784219c5aec0c79dac6->enter($__internal_e22ce1dd822dd09cd9ace9517dcfcaaaf47752dcffa20784219c5aec0c79dac6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_e22ce1dd822dd09cd9ace9517dcfcaaaf47752dcffa20784219c5aec0c79dac6->leave($__internal_e22ce1dd822dd09cd9ace9517dcfcaaaf47752dcffa20784219c5aec0c79dac6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
