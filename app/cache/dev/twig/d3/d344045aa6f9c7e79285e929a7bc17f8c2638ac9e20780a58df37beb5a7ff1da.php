<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_e528b409ca4a46513802d2b5ec3da8c01c840a6acfe7e5afa5afde8733d5673d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f603ea713a8a1cba84c10449ecb214621e9c009c671ed9827cb20b89f3f0b182 = $this->env->getExtension("native_profiler");
        $__internal_f603ea713a8a1cba84c10449ecb214621e9c009c671ed9827cb20b89f3f0b182->enter($__internal_f603ea713a8a1cba84c10449ecb214621e9c009c671ed9827cb20b89f3f0b182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_f603ea713a8a1cba84c10449ecb214621e9c009c671ed9827cb20b89f3f0b182->leave($__internal_f603ea713a8a1cba84c10449ecb214621e9c009c671ed9827cb20b89f3f0b182_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
