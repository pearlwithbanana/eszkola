<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_7ac49184649b2de025c4328dfe201f767fdcbb3f7ad11575c3e0dfdf9000ccb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7d53a97d315b15ffb0ef73e10d1c91a8378c2ea8e4945cd16a1281dc060431e = $this->env->getExtension("native_profiler");
        $__internal_f7d53a97d315b15ffb0ef73e10d1c91a8378c2ea8e4945cd16a1281dc060431e->enter($__internal_f7d53a97d315b15ffb0ef73e10d1c91a8378c2ea8e4945cd16a1281dc060431e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7d53a97d315b15ffb0ef73e10d1c91a8378c2ea8e4945cd16a1281dc060431e->leave($__internal_f7d53a97d315b15ffb0ef73e10d1c91a8378c2ea8e4945cd16a1281dc060431e_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_4c196d1492852aeb40a7919723aa4b766730990f9ce10600865a0cc0231c707b = $this->env->getExtension("native_profiler");
        $__internal_4c196d1492852aeb40a7919723aa4b766730990f9ce10600865a0cc0231c707b->enter($__internal_4c196d1492852aeb40a7919723aa4b766730990f9ce10600865a0cc0231c707b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_4c196d1492852aeb40a7919723aa4b766730990f9ce10600865a0cc0231c707b->leave($__internal_4c196d1492852aeb40a7919723aa4b766730990f9ce10600865a0cc0231c707b_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f4f7c1500d8507a61b6eafef9acc4ba161c2f17ab427229f08fe77a2b48d16db = $this->env->getExtension("native_profiler");
        $__internal_f4f7c1500d8507a61b6eafef9acc4ba161c2f17ab427229f08fe77a2b48d16db->enter($__internal_f4f7c1500d8507a61b6eafef9acc4ba161c2f17ab427229f08fe77a2b48d16db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_f4f7c1500d8507a61b6eafef9acc4ba161c2f17ab427229f08fe77a2b48d16db->leave($__internal_f4f7c1500d8507a61b6eafef9acc4ba161c2f17ab427229f08fe77a2b48d16db_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_43ebd1a03909e1682c155cc53b8dc804816e6bef2f5c9f9c8aa04741fc4948c3 = $this->env->getExtension("native_profiler");
        $__internal_43ebd1a03909e1682c155cc53b8dc804816e6bef2f5c9f9c8aa04741fc4948c3->enter($__internal_43ebd1a03909e1682c155cc53b8dc804816e6bef2f5c9f9c8aa04741fc4948c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_43ebd1a03909e1682c155cc53b8dc804816e6bef2f5c9f9c8aa04741fc4948c3->leave($__internal_43ebd1a03909e1682c155cc53b8dc804816e6bef2f5c9f9c8aa04741fc4948c3_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
