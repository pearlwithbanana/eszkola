<?php

/* :form/sys_conf:second_step.html.twig */
class __TwigTemplate_dacf952d1b8afae6585650a9aaa1e33bc2032b52be66b06eec7d8d0a939e4161 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bde886014815620126828329719c6423a6a11e5a528ebb613ec8c069ab7a2089 = $this->env->getExtension("native_profiler");
        $__internal_bde886014815620126828329719c6423a6a11e5a528ebb613ec8c069ab7a2089->enter($__internal_bde886014815620126828329719c6423a6a11e5a528ebb613ec8c069ab7a2089_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":form/sys_conf:second_step.html.twig"));

        // line 1
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
";
        // line 2
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
<ul id=\"fields-list\" data-prototype='
    ";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nameSurnameCollection", array()), "vars", array()), "prototype", array()), 'widget'));
        echo "
    ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock(twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailCollection", array()), "vars", array()), "prototype", array())), 'widget');
        echo "
    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock(twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateOfBirthCollection", array()), "vars", array()), "prototype", array())), 'widget');
        echo "</div>'>
    <button id=\"add-another-input\">Dodaj pole</button>
</ul>
";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/js/add-input.js"), "html", null, true);
        echo "\"></script>
<script>

</script>";
        
        $__internal_bde886014815620126828329719c6423a6a11e5a528ebb613ec8c069ab7a2089->leave($__internal_bde886014815620126828329719c6423a6a11e5a528ebb613ec8c069ab7a2089_prof);

    }

    public function getTemplateName()
    {
        return ":form/sys_conf:second_step.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 11,  45 => 9,  39 => 6,  35 => 5,  31 => 4,  26 => 2,  22 => 1,);
    }
}
/* {{ form_errors(form) }}*/
/* {{ form_start(form) }}*/
/* <ul id="fields-list" data-prototype='*/
/*     {{ form_widget(form.nameSurnameCollection.vars.prototype)|e }}*/
/*     {{ form_widget(form.emailCollection.vars.prototype|e) }}*/
/*     {{ form_widget(form.dateOfBirthCollection.vars.prototype|e) }}</div>'>*/
/*     <button id="add-another-input">Dodaj pole</button>*/
/* </ul>*/
/* {{ form_end(form) }}*/
/* <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>*/
/* <script src="{{ asset('assets/js/add-input.js') }}"></script>*/
/* <script>*/
/* */
/* </script>*/
