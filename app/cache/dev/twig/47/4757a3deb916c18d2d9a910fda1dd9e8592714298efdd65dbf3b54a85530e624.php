<?php

/* form/new_password.html.twig */
class __TwigTemplate_fb3fd14857fbd0f80745d7b96db4b08a8c63c5da62be0ad239009671d376f9d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19bcb31439596f66cb002fd4c18ba41c21d5ab013c48da04a6efbe709e5af8bb = $this->env->getExtension("native_profiler");
        $__internal_19bcb31439596f66cb002fd4c18ba41c21d5ab013c48da04a6efbe709e5af8bb->enter($__internal_19bcb31439596f66cb002fd4c18ba41c21d5ab013c48da04a6efbe709e5af8bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/new_password.html.twig"));

        // line 1
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 2
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        
        $__internal_19bcb31439596f66cb002fd4c18ba41c21d5ab013c48da04a6efbe709e5af8bb->leave($__internal_19bcb31439596f66cb002fd4c18ba41c21d5ab013c48da04a6efbe709e5af8bb_prof);

    }

    public function getTemplateName()
    {
        return "form/new_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  26 => 2,  22 => 1,);
    }
}
/* {{ form_start(form) }}*/
/* {{ form_widget(form) }}*/
/* {{ form_end(form) }}*/
