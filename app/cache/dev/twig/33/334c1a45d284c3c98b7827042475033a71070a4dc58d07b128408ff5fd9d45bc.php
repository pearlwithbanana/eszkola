<?php

/* :message:success_registration.html.twig */
class __TwigTemplate_84ed130d62f49f528c1e3b75bf4aa74c96f8551b1b2e88ce66e23bddbb4ed437 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'success_registration_message' => array($this, 'block_success_registration_message'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80f7f95971daaac356614560543ea85be1f237c4a85f1f13d80713232f1f79e2 = $this->env->getExtension("native_profiler");
        $__internal_80f7f95971daaac356614560543ea85be1f237c4a85f1f13d80713232f1f79e2->enter($__internal_80f7f95971daaac356614560543ea85be1f237c4a85f1f13d80713232f1f79e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":message:success_registration.html.twig"));

        // line 1
        $this->displayBlock('success_registration_message', $context, $blocks);
        
        $__internal_80f7f95971daaac356614560543ea85be1f237c4a85f1f13d80713232f1f79e2->leave($__internal_80f7f95971daaac356614560543ea85be1f237c4a85f1f13d80713232f1f79e2_prof);

    }

    public function block_success_registration_message($context, array $blocks = array())
    {
        $__internal_8426d16dc3e0f8cc30d0896c05641c790d99a924348a4e1c9e3007bb86838cc7 = $this->env->getExtension("native_profiler");
        $__internal_8426d16dc3e0f8cc30d0896c05641c790d99a924348a4e1c9e3007bb86838cc7->enter($__internal_8426d16dc3e0f8cc30d0896c05641c790d99a924348a4e1c9e3007bb86838cc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "success_registration_message"));

        // line 2
        echo "    Na adres ";
        echo twig_escape_filter($this->env, (isset($context["mail"]) ? $context["mail"] : $this->getContext($context, "mail")), "html", null, true);
        echo " została wysłana wiadomość z linkiem aktywacyjnym. Użytkownik będzie mógł zalogować się do systemu po wcześniejszej aktywacji konta.
";
        
        $__internal_8426d16dc3e0f8cc30d0896c05641c790d99a924348a4e1c9e3007bb86838cc7->leave($__internal_8426d16dc3e0f8cc30d0896c05641c790d99a924348a4e1c9e3007bb86838cc7_prof);

    }

    public function getTemplateName()
    {
        return ":message:success_registration.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  35 => 2,  23 => 1,);
    }
}
/* {% block success_registration_message %}*/
/*     Na adres {{ mail }} została wysłana wiadomość z linkiem aktywacyjnym. Użytkownik będzie mógł zalogować się do systemu po wcześniejszej aktywacji konta.*/
/* {% endblock %}*/
/* */
