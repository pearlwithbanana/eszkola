<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_d656d93587c6b716f068d3af758f68bdc5213df3596321765780c0a2c138fb9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_69be3e3c4cf0445cb492f68e44699b265c6cd60b827db952e0e07179d4524d13 = $this->env->getExtension("native_profiler");
        $__internal_69be3e3c4cf0445cb492f68e44699b265c6cd60b827db952e0e07179d4524d13->enter($__internal_69be3e3c4cf0445cb492f68e44699b265c6cd60b827db952e0e07179d4524d13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_69be3e3c4cf0445cb492f68e44699b265c6cd60b827db952e0e07179d4524d13->leave($__internal_69be3e3c4cf0445cb492f68e44699b265c6cd60b827db952e0e07179d4524d13_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
