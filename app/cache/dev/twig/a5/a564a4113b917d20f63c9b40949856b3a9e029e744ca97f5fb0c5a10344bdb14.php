<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_da27d7675b3c6578192bba69f91ef3c75b11fe1fc9edcf9763d52ba223bec8dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_be70c041e32bbee16d6dfad29d536989b09a45e669ae25df3c96473c854926b2 = $this->env->getExtension("native_profiler");
        $__internal_be70c041e32bbee16d6dfad29d536989b09a45e669ae25df3c96473c854926b2->enter($__internal_be70c041e32bbee16d6dfad29d536989b09a45e669ae25df3c96473c854926b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_be70c041e32bbee16d6dfad29d536989b09a45e669ae25df3c96473c854926b2->leave($__internal_be70c041e32bbee16d6dfad29d536989b09a45e669ae25df3c96473c854926b2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
