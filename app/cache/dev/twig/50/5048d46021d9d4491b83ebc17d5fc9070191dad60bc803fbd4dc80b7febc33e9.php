<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_b481d6ab072bdef0fa2c124f32c611e0e69b5cae35a97a17ac0d061ba8fd3892 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3c52fed65753fa5fd1e5050c209c8af6a5f68720a27829645d2e06905ed1052 = $this->env->getExtension("native_profiler");
        $__internal_c3c52fed65753fa5fd1e5050c209c8af6a5f68720a27829645d2e06905ed1052->enter($__internal_c3c52fed65753fa5fd1e5050c209c8af6a5f68720a27829645d2e06905ed1052_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_c3c52fed65753fa5fd1e5050c209c8af6a5f68720a27829645d2e06905ed1052->leave($__internal_c3c52fed65753fa5fd1e5050c209c8af6a5f68720a27829645d2e06905ed1052_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
