<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'oldPassword',
                'password',
                array(
                    'attr' => array(
                        'placeholder' => 'Podaj stare hasło',
                    ),
                    'label' => false,
                )
            )
            ->add(
                'plainPassword',
                'repeated',
                array(
                    'invalid_message' => 'Podane hasła są różne',
                    'type' => 'password',
                    'first_options' => array(
                        'attr' => array('placeholder' => 'Wpisz nowe hasło'),
                        'label' => false,
                    ),
                    'second_options' => array(
                        'attr' => array('placeholder' => 'Powtórz nowe hasło'),
                        'label' => false,
                    ),
                )
            )
            ->add('Zmień hasło', 'submit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\User',
            )
        );
    }

    public function getName()
    {
        return 'change_password';
    }
}
