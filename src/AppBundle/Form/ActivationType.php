<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActivationType extends AbstractType
{
    private $userdata;

    public function __construct(array $userdata) {
        $this->userdata = $userdata;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'Podane hasła są różne',
            'required' => true,
            'first_options'  => array('label' => 'Ustaw hasło'),
            'second_options' => array('label' => 'Powtórz hasło')
        ));
           if($this->userdata['phoneNumber'] === null) {
               $builder->add(
                   'phoneNumber',
                   'text',
                   array(
                       'attr' => array(
                           'minLength' => 9,
                           'maxLength' => 9,
                       ),
                       'label' => "Numer telefonu",
                       'required' => false,
                   ));
           }
            $builder->add('Aktywuj konto', 'submit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'app_bundle_activation_type';
    }
}
