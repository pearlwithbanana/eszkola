<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RemindPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailOrLogin', 'text', array(
                'attr' => array(
                    'placeholder' => "Adres email lub login",
                ),
                'label' => false,
        ))
            ->add('Przypomnij hasło', 'submit');
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'remind_password_type';
    }
}
