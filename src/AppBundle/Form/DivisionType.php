<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DivisionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'divisions',
                'collection',
                array(
                    'options' => array(
                        'attr' => array(
                            'class' => 'division-name',
                            'placeholder' => "Wpisz nazwę klasy",
                            /*
                             * If you ever decide to disable this HTML validation constrains,
                             * you MUST read the comment over the
                             * AppBundle:Entity:Division;divisions property!
                             * So don't disable it if it's not necessary.
                             */
                            'minLength' => "1",
                            'maxLength' => "4",
                        ),
                    ),
                    'required' => true,
                    'type' => 'text',
                    'label' => "Kretor klas:",
                    'allow_add' => true,
                    'allow_delete' => true,
                    'delete_empty' => true,
                    //don't even try to set above prototype to false
                    // or you'll be punished with a big dick right in your ass
                    'prototype' => true,
                )
            )
            ->add('Zatwierdź', "submit");
    }

    public function getName()
    {
        return "division";
    }
}