<?php
namespace AppBundle\Form;

use AppBundle\Roles\UserRoles;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    private $availableToAddRoles;
    public $basicUserBuilder;

    public function __construct($availableToAddRoles)
    {
        $this->availableToAddRoles = $availableToAddRoles;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameSurnameCollection', 'collection', array(
                'options' => array(
                    'attr' => array(
                        'placeholder' => "Imię i nazwisko",
                        'minLength' => "2",
                        'maxLength' => "32",
                    )),
                'required' => true,
                'type' => 'text',
                'label' => "Dodaj nauczycieli:",
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                //don't even try to set above prototype to false
                // or you'll be punished with a big dick right in your ass
                'prototype' => true,
                ))
            ->add('')
            ->add('dateOfBirth', 'birthday', array(
                'label' => "Wybierz datę urodzenia użytkownika",
                'placeholder' => array('year' => 'Rok', 'month' => 'Miesiąc', 'day' => 'Dzień'),
                'format' => 'dd-MM-yyyy',
                'invalid_message' => "Podano niewłaściwy format daty"
            ));
        if ($this->availableToAddRoles) {
            $builder->add('plainRoles', 'choice', array(
                'choices' => $this->availableToAddRoles,
                'label' => "Wybierz typ użytkownika",
                'placeholder' => "Użytkownik"
                ));
        }
        $builder->add('Zarejestruj', 'submit');

        $this->basicUserBuilder = $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'userr';
    }
}
