<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormFactoryInterface;

class BasicUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'fullName',
                'text',
                array(
                    'attr' => array(
                        'placeholder' => "Imię i nazwisko",
                    ),
                    'label' => false,
                    'required' => false,
                )
            )
            ->add(
                'email',
                'email',
                array(
                    'attr' => array(
                        'placeholder' => "Adres email",
                    ),
                    'label' => false,
                )
            )
            ->add(
                'phoneNumber',
                'text',
                array(
                    'attr' => array(
                        'placeholder' => "Podaj numer telefonu",
                        'minLength' => 9,
                        'maxLength' => 9,
                    ),
                    'label' => false,
                    'required' => false,
                )
            )
            ->add(
                'dateOfBirth',
                'birthday',
                array(
                    'label' => "Wybierz datę urodzenia użytkownika",
                    'placeholder' => array('year' => 'Rok', 'month' => 'Miesiąc', 'day' => 'Dzień'),
                    'format' => 'dd-MM-yyyy',
                    'years' => range(date('Y') - 6, date('Y') - 67),
                    'invalid_message' => "Podano niewłaściwy format daty",
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\User',
//                'attr' => array('novalidate' => 'novalidate'),
            )
        );
    }

    public function getName()
    {
        return 'user';
    }
}
