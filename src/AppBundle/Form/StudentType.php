<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 03.11.15
 * Time: 19:16
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class StudentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('studentDivision', 'entity', array(
            'class' => 'AppBundle:Division',
            'property' => 'divisionName',
            'label' => "Klasa"
        ));
    }

    public function getName()
    {
        return "student";
    }
}