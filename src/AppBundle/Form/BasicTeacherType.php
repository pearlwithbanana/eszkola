<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BasicTeacherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'users',
                'collection',
                array(
                    'type' => new BasicUserType(),
                    'label' => false,
                    'allow_add' => true,
                    'delete_empty' => true,
                )
            )
            ->add('add_teacher', 'submit', array('label' => "Dodaj nauczyciela"));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Teacher',
//                'attr' => array('novalidate' => 'novalidate'),
            )
        );
    }

    public function getName()
    {
        return 'teacher';
    }
}
