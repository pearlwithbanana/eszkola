<?php

namespace AppBundle\Form;

use AppBundle\Entity\Division;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EducatorType extends AbstractType
{
    private $user;
    private $division;

    public function __construct(EntityRepository $user, EntityRepository $division)
    {
        $this->user = $user;
        $this->division = $division;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $qb = $this->division->createQueryBuilder('d')->select('COUNT(d)');
//        $numberOfDivisions = $qb->getQuery()->getSingleScalarResult();
//        $divisions = $this->division->findAll();
//
//        foreach ($divisions as $division) {
//            $builder
//                ->add(
//                    $division->getDivisionName(),
//                    EntityType::class,
//                    array(
//                        'label' => $division->getDivisionName(),
//                        'class' => 'AppBundle:Teacher',
//                        'choice_label' => function ($teacher) {
//                            return $teacher->getTeacherFullNameAndEmail();
//                        },
//                    )
//                );
//        }
//        $builder->add('Dodaj wychowawców', 'submit');
        $builder->add(
            'teacher',
            EntityType::class,
            array(
                'class' => 'AppBundle:Teacher',
                'choice_label' => function ($teacher) {
                    return $teacher->getTeacherFullNameAndEmail();
                },
                'label' => false,
            )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'AppBundle\Entity\Educator'));
    }

    public function getName()
    {
        return 'app_bundle_teachers_to_divisions_type';
    }
}
