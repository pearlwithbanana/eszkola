<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'plainPassword',
            'repeated',
            array(
                'type' => 'password',
                'invalid_message' => 'Podane hasła są różne',
                'required' => true,
                'first_options' => array(
                    'attr' => array(
                        'placeholder' => 'Wpisz swoje nowe hasło',
                    ),
                    'label' => false,
                ),
                'second_options' => array(
                    'attr' => array(
                        'placeholder' => 'Powtórz swoje nowe hasło',
                    ),
                    'label' => false,
                ),
            )
        )
            ->add('oldPassword','hidden', array('mapped' => false))

            ->add("Zmień hasło", "submit");
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'AppBundle\Entity\User'));
    }

    public function getName()
    {
        return 'new_password_type';
    }
}
