<?php

namespace AppBundle\Mapper;

use Monolog\Handler\Curl\Util;

class RoleMapper
{
    private $entityManager;
    private $userType;
    private $availableToAddRoles;

    public function __construct($entityManager, $userType)
    {
        $this->entityManager = $entityManager;
        $this->userType = $userType;
        $this->availableToAddRoles = $this->checkAvailableRoles();
    }

    private function checkAvailableRoles()
    {
        $userType = $this->userType;
        if ($userType === "ROLE_HEADMASTER") {
            return
                $this
                    ->entityManager
                    ->getConnection()
                    ->executeQuery(
                        "SELECT role_id, user_friendly_role_name FROM user_role WHERE required_level_to_add = 0 OR required_level_to_add = 1 ORDER BY role_id DESC;"
                    )
                    ->fetchAll(\PDO::FETCH_KEY_PAIR);
        } else if ($userType === "ROLE_ACCOUNTANT") {
            return $this
                ->entityManager
                ->getConnection()
                ->executeQuery(
                    "SELECT role_id, user_friendly_role_name FROM user_role WHERE required_level_to_add = 0 ORDER BY role_id DESC;"
                )
                ->fetch(\PDO::FETCH_BOUND);
        } else {
            throw new \LogicException('\AppBundle\Mapper\RoleMapper: invalid value in __construct(). Expected "ROLE_HEADMASTER" OR "ROLE_ACCOUNTANT');
        }
    }

//    public function getRoleIdByRoleName($roleName) {
//        $row =  $this->entityManager->getConnection()->executeQuery(
//            "SELECT role_id FROM user_role WHERE user_friendly_role_name = ".$roleName
//        )->fetchColumn();
//        return $row;
//    }

    public function getAvailableToAddRoles()
    {
        return $this->availableToAddRoles;
    }
}