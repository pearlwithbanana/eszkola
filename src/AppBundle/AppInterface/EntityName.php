<?php

/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 30.11.15
 * Time: 18:27
 */

namespace AppBundle\AppInterface;

interface EntityName
{
    public function getEntityName();
}