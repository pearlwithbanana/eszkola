<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Mail\Mailsender;
use AppBundle\Entity\User;
use AppBundle\Mapper\RoleMapper;
//use AppBundle\Helper\StringHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class RegistrationController extends Controller
{
    private $newUser;
    private $entityManager;
    private $roleMapper;

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {
        $this->entityManager = $this->getDoctrine()->getManager();
        $availableToAddRoles = null;

        if ($this->isGranted('ROLE_HEADMASTER')) {
            $this->roleMapper = new RoleMapper($this->entityManager, 'ROLE_HEADMASTER');
            $availableToAddRoles = $this->roleMapper->getAvailableToAddRoles();
        }
        if ($this->isGranted('ROLE_ACCOUNTANT')) {
            $this->roleMapper = new RoleMapper($this->entityManager, 'ROLE_ACCOUNTANT');
            $availableToAddRoles = $this->roleMapper->getAvailableToAddRoles();
        }


        $this->newUser = new User();
        $form = $this->createForm(
            new UserType($availableToAddRoles), $this->newUser
        );

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) { // &&userRolesAreValid -> instance of validators ValidRoles(currentuser) [mapped roles]
            //$this->get('security.password_encoder')->encodePassword($newUser, $newUser->getPlainPassword());
            $this->persistUser();
            $email = $form->get('email')->getData();

            //@TODO mail aktywacyjny
            // tutaj wyslij maila z $hash
//            $mailSend = new MailSender($hash, $email);
//            $mailSend->SendActivationMail();
            return $this->redirect('/success_registration/' . $email);
        }
        $errors = $form->getErrors();
        return $this->render('form/register.html.twig', array('form' => $form->createView(), 'errors' => $errors));
    }

    /**
     * @Route("/success_registration/{email}")
     */
    
    public function SuccessRegistrationAction($email)
    {
        return $this->render('message/success_registration.html.twig', array('mail' => $email));
    }

    private function persistUser() {

    }
}
