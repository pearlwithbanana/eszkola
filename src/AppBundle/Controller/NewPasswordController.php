<?php

namespace AppBundle\Controller;

use AppBundle\Form\NewPasswordType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class NewPasswordController extends Controller
{
    /**
     * @Route("/new_password/{link}", name="new_password")
     */

    public function newPasswordAction(Request $request, $link)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('AppBundle:User');
        $user = $userRepository->findOneBy(array("mailHash" => $link));
        $dateDifference = $this->get('count_date_difference');
        $daysDifference = $dateDifference->getDateDifference("2015-12-06", "2015-12-03");

        if ($daysDifference > 14 || $user === null) {
            return $this->render('message/link_expired.html.twig');
        }

        $form = $this->createForm(new NewPasswordType(), $user);
        $userMail = $user->getEmail();

        $user->setEmail("somethingiswrongwithemailvalidationmuszetozglosicnapolicje@fmrqweimqwie.pl");

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $user->setEmail($userMail);
            $user->setMailHash(null);
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $em->persist($user);
            $em->flush();

            $this->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();


            return $this->render("message/password_changed.html.twig");
        }

        $errors = $form->getErrors();
        return $this->render("form/new_password.html.twig", array(
            "form" => $form->createView(),
            "errors" => $errors));
    }
}
