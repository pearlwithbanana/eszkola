<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\ActivationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ActivationController extends Controller
{

    private $username;

    /**
     * @Route("/user_activation/{link}", name="user_activation")
     */
    public function activationAction(Request $request, $link)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('AppBundle:User');
        $user = $userRepository->findOneBy(array("mailHash" => $link));

        if($user === null) {
            return $this->render('message/link_expired.html.twig');
        }

        $userdata = array("phoneNumber" => $user->getPhoneNumber());
        $form = $this->createForm(new ActivationType($userdata), $user);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $this->username = $user->getUsername();

            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setIsActive(true);
            $user->setMailHash(null);
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('success_activation');

        }

        $errors = $form->getErrors();
        return $this->render('form/activate_user.html.twig', array('form' => $form->createView(), 'errors' => $errors));
    }

    /**
     * @Route("/success_activation", name="success_activation")
     */
    public function successActivationAction(Request $request)
    {
        $session = $request->getSession();
        $lastRouteName = $session->get('last_route')['name'];

        if($lastRouteName !== 'user_activation') {
            throw new AccessDeniedHttpException();
        }

        return $this->render('message/success_activation.html.twig', array("username" => $this->username));
    }
}
