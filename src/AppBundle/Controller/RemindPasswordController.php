<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\RemindPasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class RemindPasswordController extends Controller
{
    /**
     * @Route("/remind_password", name="remind_password")
     */
    public function remindPasswordAction(Request $request)
    {
        $form = $this->createForm(new RemindPasswordType());
        $form->handleRequest($request);
        $emailOrLogin = $form->getData()['emailOrLogin'];
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT u FROM AppBundle:User u WHERE u.username = :emailOrLogin OR u.email = :emailOrLogin'
        )
            ->setParameter('emailOrLogin', $emailOrLogin);

        $result = $query->getResult();

        if ($result) {
            $user = $result[0];
        } else if($emailOrLogin !== null) {
            $form->addError(new FormError("Użytkownik o podanych danych nie istnieje"));
        }

        if ($form->isValid() && $form->isSubmitted()) {

            $hash = $this->get('generate_hash');
            $mailHash = $hash->generateHash();
            $user->setMailHash($mailHash);
            $em->persist($user);
            $em->flush();

            $mailer = $this->get('send_mail');
            $mailer->send(
                'mail/remind_password.html.twig',
                'Przypomnienie hasła dla konta '.$user->getUsername(),
                $user->getEmail(),
                array(
                    'username' => $user->getUsername(),
                    'mailHash' => $user->getMailHash(),
                )
            );

            /*
             * @TODO: Check what's the user mailbox URL and redirect to it after a banner and certain amount of time :)
             */

            return $this->render('message/success_password_remind.html.twig', array('email' => $user->getEmail()));

        }

        $errors = $form->getErrors();

        return $this->render(
            'form/remind_password.html.twig',
            array('form' => $form->createView(), 'errors' => $errors)
        );
    }
}

/**
 * Zenon Ziębiewicz oskarżenie i obrona "Granica"
 *
 * Oskarżenie:
 *
 * Zenon był winien swojemu losowi. Do jego klęski doporowadziła go hipokryzja i nieporadność, którą
 * wiecznie tłumaczył swoją życiową nieporadność. Jego pochodzenie nie usprawiedliwia podwójnej natury
 * i manipulowania ludzkimi czynami, uczuciami. Dobitnym dowodem jego winy jest sposób, w który
 * kryje swoje ater ego. Egoizm Zenona doprowadził do morderstwa nienarodzonego dziecka. Jego nieludzkość
 * nawet nie pozwoliła mu żałować swojego czynu. To jak traktował swoją narzeczoną, a później żonę Elżbietę
 * także pozostawia wiele do życzenia. Jako ojciec przez pryzmat włansego rodzica powinien chronić swoje
 * dziecko, Walerian musiał dojrzewać patrząc na poczyniania ojca. Zenon nieludzko traktował także
 * Adelę - chorą na gruźlicę kochankę, do której czuł jedynie pożądanie.
 *
 * Obrona:
 *
 * Zenon nie do końca powinine otrzymać karę, na którą został skazany. Był osobą o bardzo silnym charakterze
 * i motywacji. Mimo ciężkiej sytuacji finansowej skończł studia w Paryżu, zrobił karierę i stał się wzorem
 * do naśladowania dla wielu osób. Bardzo żałował, ze odziedziczył geny ojca, które w konsekwencji niszczyły
 * osobowość, którą tak ciężko budował. Zawsze starał się naprawiać swoije błędy, które popełniał pod wpływem
 * chwili, nie do końca świadomie. Nigdy nie myślał o konsekwencjach, a zdarzenia początkowo nie były zaplanowane.
 * Zawsze starał się być osobą szczerą, czy to mówiąc żonie o romansie, czy Adeli, że jej nie kocha.
 * Justynie mimo tego jak ją potraktował pomógł znaleźć pracę i wejść w lepsze życie.
 * Uważam, że to naparstek czynów, które usprawiedliwiają naturę, z którą nie mógł sobie do końca poradzić.
 *
 * Boże jak ja kocham język polski i moją nauczycielkę.
 */