<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 06.11.15
 * Time: 10:02
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\StudentType;
use AppBundle\Entity\Student;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{
    /**
     * @Route("test")
     */

    public function testAction(Request $request) {
        $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom("eszkola.service@gmail.com")
            ->setTo("ziemniaczek10@gmail.com")
            ->setBody(
                "dupa"
            );
        $this->get('mailer')->send($message);

        return new Response("test");
    }
}
