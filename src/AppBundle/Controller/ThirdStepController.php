<?php

namespace AppBundle\Controller;

use AppBundle\Form\EducatorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Teacher;

class ThirdStepController extends Controller
{
    /**
     * @Route("/configuration_step_3")
     */
    public function thirdStepAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $division = $em->getRepository("AppBundle:Division");
        $teacher = $em->getRepository("AppBundle:Teacher");

        $form = $this->createForm(new EducatorType($teacher, $division));
        $form->handleRequest($request);

//        $this->saveRelations($form->getData());

        if($form->isValid() && $form->isSubmitted()) {

        }

        $errors = $form->getErrors();
        return $this->render('form/sys_conf/third_step.html.twig', array('form' => $form->createView(), 'errors' => $errors));
    }

    public function saveRelations(array $data) {
        foreach($data as $inversedSite => $mappedSite) {

        }
    }
}
