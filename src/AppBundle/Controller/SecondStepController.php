<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 16.11.15
 * Time: 15:36
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\DivisionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Teacher;
use AppBundle\Form\TeacherType;
use AppBundle\Form\BasicUserType;
use AppBundle\Form\BasicTeacherType;

class SecondStepController extends Controller
{
    /**
     * @Route("/configuration_step_2", name="configuration_step_2")
     */
    public function secondStepAction(Request $request)
    {

        $teacher = new Teacher();
        $user = new User();
        $teacher->getUsers()->add($user);
        $form = $this->createForm(new BasicTeacherType(), $teacher);
        $form->handleRequest($request);


        if ($form->isValid() && $form->isSubmitted()) {

            $persistUserService = $this->get('persist_user');
            foreach ($teacher->getUsers() as $user) {
                $persistUserService->persistUser($user, new Teacher());
            }

            $nextStep = $this->get('next_configuration_step');
            $nextStep->nextConfigurationStep();
            $url = $nextStep->getNextConfigurationStepUrl();

            return $this->redirect($url);

        }

        $errors = $form->getErrors();

        return $this->render(
            'form/add_teacher',
            array('form' => $form->createView(), 'errors' => $errors)
        );
    }
}