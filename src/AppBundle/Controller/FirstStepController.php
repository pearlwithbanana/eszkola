<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Division;
use AppBundle\Form\DivisionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;


class FirstStepController extends Controller
{
    private $division;

    /**
     * @Route("/configuration_step_1", name="configuration_step_1")
     */
    public function firstStepAction(Request $request) {

        $this->division = new Division();
        $this->division->divisions->add("");
        $form = $this->createForm(new DivisionType(), $this->division);

//        $nextStepService = $this->get('next_configuration_step');
//        $url = $nextStepService->nextConfigurationStep();
//        var_dump($url);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()) {
            $this->addRows($this->division->divisions);
            $nextStepService = $this->get('next_configuration_step');
            $nextStepService->NextConfigurationStep();
            $url = $nextStepService->getNextConfigurationStepUrl();
            return $this->redirect($url);
        }

        $errors = $form->getErrors();
        return $this->render('form/sys_conf/first_step.html.twig', array('form' => $form->createView(), 'errors' => $errors));
    }

    private function addRows($collection) {

        $em = $this->getDoctrine()->getManager();
        foreach($collection as $row) {
            //add division only if it doesn't exist
            if(!($em->getRepository('AppBundle:Division')->findOneBy(array('divisionName' => $row)))) {
                $object = new Division();
                //delete white characters and make division name uppercase
                $object->setDivisionName(str_replace(" ", "", strtoupper($row)));
                $em->persist($object);
                $em->flush();
            }
        }
    }
}