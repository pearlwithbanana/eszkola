<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request) {
        /**
         * If you'll have (I wish you would not) an "authentication failed due to a system problem" error,
         * uncomment the above dump to see what's you error.
         * Probably it's because you haven't updated the database with doctrine:schema:update --force command yet
         * or you have logical ORM error in your entity
         */
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('home');
        }

        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('form/login.html.twig', array(
            'error' => $error,
            'last_username' => $lastUsername
            ));
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction() {

    }
}