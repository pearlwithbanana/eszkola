<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class ChangePasswordController extends Controller
{
    /**
     * @Route("/change_password", name="change_password")
     */
    public function changePasswordAction(Request $request)
    {
        return $this->render("form/change_password.html.twig");

    }

    /**
     * @Route("/change_password_checker", name="change_password_checker")
     */
    public function changePasswordChecker() {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(!$user) {
            return new Response("Sesja wygasła");
        }
        $hashService = $this->get('generate_hash');
        $user->setMailHash($hashService->generateHash($user->getId()));
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $mailer = $this->container->get('send_mail');
        $data = array("username" => $user->getUsername(), "hashLink" => $user->getMailHash());

        $mailer->send(
            "mail/new_password.html.twig",
            "Nowe hasło dla użytkownika ".$user->getUsername(),
//            "ziemniaczek10@gmail.com",
            $user->getEmail(),
            $data
        );
        return $this->render('message/success_password_change.html.twig');
    }
}
