<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 03.12.15
 * Time: 20:18
 */

namespace AppBundle\Service;


class GenerateHash
{
    public function generateHash($id = 0) {

        $plainHash =
            rand(0,100).$id."!%(@XwA454@".$id+rand(0,100).md5(rand(0,100));

        $hash = sha1($plainHash);

        return $hash;
    }
}