<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 03.12.15
 * Time: 21:39
 */

namespace AppBundle\Service;

class CountDateDifference
{
    /**
     * The method returns an integer value of difference between some point in two times.
     * As you can see, the default start point is the epoch date.
     */
    public function getDateDifference($endDate, $startDate = "1970-01-01", $returnsHours = true)
    {
        $start = new \DateTime($startDate);
        $end = new \DateTime($endDate);

        $differenceInSeconds = $start->format('U') - $end->format('U');
        if (!$returnsHours) {
            return $differenceInSeconds;
        }

        $secondsInDay = 86400;

        /*
         * hoho, I'm pretty sure you haven't known abs() function before!
         * It's me who has found it on the end of internet - it returns the absolute
         * value of a number. Why am I using it? What if me or some other moron forget
         * about the arguments order?
         */
        $differenceInHours = abs((int)round($differenceInSeconds / $secondsInDay));
        return $differenceInHours;
    }
}