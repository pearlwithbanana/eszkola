<?php

namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;

class NextConfigurationStep
{
    private $em;
    private $firstRunRepository;
    private $currentStep;
    private $configuration_path = "/configuration_step_";
    private $configurationSteps = 2;

    //firstRunRepositoryPath is configurable and can be changed in the app/config/services.yml file
    public function __construct($firstRunRepositoryPath, EntityManager $entityManager) {
        $this->em = $entityManager;
        $this->firstRunRepository = $this->em->getRepository($firstRunRepositoryPath)->find(1);
        $this->currentStep = $this->firstRunRepository->getCurrentConfigurationStep();
    }

    /**
     * @set increments current configuration step in database by one (+)(+)
     * @return url string which can be used to redirect to next configuration step
     */
    public function nextConfigurationStep() {

        $currentStep = $this->getCurrentStep();

        if($currentStep) { //could be null
            $nextStep = $currentStep + 1;

            //if the user got through all configuration steps set system to cofigured
            if($nextStep > $this->configurationSteps) {
                $nextStep = null;
            }

            $this->firstRunRepository->setCurrentConfigurationStep($nextStep);
            $this->em->persist($this->firstRunRepository);
            $this->em->flush();

            return $this->getNextConfigurationStepUrl();
        }
        return null;
    }

    public function getCurrentConfigurationStepUrl() {
        return $this->configuration_path.$this->currentStep;
    }

    public function getNextConfigurationStepUrl() {
        if(null === $this->currentStep && $this->currentStep > $this->configurationSteps) {
            return "/home";
        }

        return $this->configuration_path.++$this->currentStep;
    }

    public function getCurrentStep() {
        return $this->currentStep;
    }
}