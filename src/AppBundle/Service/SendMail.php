<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 28.11.15
 * Time: 21:55
 */

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class SendMail
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function send(
        $contentPath,
        $subject,
        $recipient,
        $data = null,
        $sender = "eszkola.service@gmail.com"
    ) {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($sender)
            ->setTo($recipient)
            ->setBody(
                $this->container->get('templating')->render(
                    $contentPath,
                    array("data" => $data)
                ),
                'text/html'
            );
        $this->container->get('mailer')->send($message);
    }

}