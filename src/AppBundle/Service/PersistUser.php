<?php

namespace AppBundle\Service;

use AppBundle\Form\BasicUserType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Test\FormBuilderInterface;
use AppBundle\AppInterface\EntityName;

//BIG @TODO: make service abstract for every kind of user
/*
 * Referenced future services:
 * - ReferencedObjectsMapper
 */

class PersistUser
{
    private $userRepository;
    private $container;
    private $entityManager;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $entityManager->getRepository('AppBundle:User');
        $this->container = $container;
    }

    public function persistUser(User $user, $userType)
    {
        $userNames = $this->getUserNames($user->getFullName());
        $user->setFullName($userNames['firstName']." ".$userNames['lastName']);
        $username = $this->generateNick($userNames['firstName'], $userNames['lastName']);
        $user->setUsername($username);
        $hashLink = $this->getRegistrationHash($username);
        $user->setMailHash($hashLink);
        $role = $this->getRole($userType);
        $user->setRoles($role);
        $userType->setUser($user);

        $this->entityManager->persist($user);
        $this->entityManager->persist($userType);
        $this->entityManager->flush();

        $this->sendActivationMail(
            $user->getEmail(),
            $username,
            $hashLink
        );
    }

    private function generateNick($firstName, $lastName)
    {
        $firstName = \AppBundle\Helper\StringHelper::ReplaceDiacritics($firstName);
        $lastName = \AppBundle\Helper\StringHelper::ReplaceDiacritics($lastName);

        $nick = substr($firstName, 0, 3).substr($lastName, 0, 3);
        $nick = strtolower($nick).rand(100, 999);;
        if ($this->userRepository->findBy(array('username' => $nick))) {
            $this->generateNick($firstName, $lastName);
        }

        return $nick;
    }

    private function getUserNames($fullName)
    {
        $names = explode(" ", $fullName);
        foreach ($names as &$name) {
            for ($i = 0; $i < strlen($name); $i++) {
                if ($name[$i] === " ") {
                    $name = substr($name, $i, 1);
                }
            }
        }

        $result = array();
        $result['firstName'] = ucfirst($names[0]);
        $result['lastName'] = "";

        for ($i = 1; $i < count($names); $i++) {
            $result['lastName'] .= ucfirst($names[$i])." ";
        }

        $result['lastName'] = trim($result['lastName']);

        return $result;
    }

    private function sendActivationMail($email, $username, $hashLink)
    {
        $mailer = $this->container->get('send_mail');
        $data = array("username" => $username, "hashLink" => $hashLink);
        $mailer->send(
            "mail/registration.html.twig",
            "Rejestracja użytkownika w systemie szkoły elektronicznej",
            $email,
            $data
        );
    }

    private function getRegistrationHash($username)
    {
        $plainHash = mt_rand(0, 1000)."somerandomwordsreqwciendandl4132391432nfiewrn".$username;
        $hash = sha1($plainHash);

        return $hash;
    }

    private function getRole(EntityName $user) {

        $roleName = "ROLE_". strtoupper($user->getEntityName());

        $roleRepository = $this->entityManager->getRepository('AppBundle:Role');
        $role = $roleRepository->findOneBy(array("role" => $roleName));

        return $role;
    }
}
