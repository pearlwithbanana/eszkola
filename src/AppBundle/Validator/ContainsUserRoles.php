<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 28.10.15
 * Time: 21:33
 */

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

class ContainsUserRoles extends Constraint
{
    public $message = 'Formularz został odrzucony przez serwer z powodu niespójności danych';
}