<?php

namespace AppBundle\Validator;

use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use AppBundle\Roles\UserRoles;

class ContainsUserRolesValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        $userRoles = new UserRoles();
        //@TODO: current usr obj
        $userObject = new User();

        if($userObject->getRoles() === 'ROLE_HEADMASTER') {
            $availableRoles = $userRoles->getHeadmasterRoles;
        }
        else if($userObject->GetRoles() === 'ROLE_SECRETARY') {
            $availableRoles = $userRoles->getSecreataryRoles;
        } else {
            return false;
        }

        foreach($userObject->getRoles() as $role) {
            if(!in_array($role, $availableRoles)) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
        return true;
    }
}