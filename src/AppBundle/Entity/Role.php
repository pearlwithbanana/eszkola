<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 28.10.15
 * Time: 19:17
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_role")
 */
class Role implements RoleInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="role_id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="role_name", type="string", length=32)
     * @ORM\OneToMany(targetEntity="User", mappedBy="roles")
     */
    private $role;

    /**
     * @ORM\Column(name="user_friendly_role_name", type="string", length=32)
     */
    private $userFriendlyRoleName;

    /**
     * @ORM\Column(name="required_level_to_add", type="integer")
     * level 0 - ROLE_ACCOUNTANT is available to those roles
     * level 1 - ROLE_ROOT, ROLE_HEADMASTER is available to add those roles
     */
    private $requiredLevelToAdd = 0;

    public function getId()
    {
        return $this->id;
    }

    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    public function setRequiredLevelToAdd($requiredLevelToAdd)
    {
        $this->requiredLevelToAdd = $requiredLevelToAdd;
    }

    public function getRequiredLevelToAdd()
    {
        return $this->requiredLevelToAdd;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setUserFriendlyRoleName($userFriendlyRoleName)
    {
        $this->userFriendlyRoleName = $userFriendlyRoleName;

        return $this;
    }

    public function getUserFriendlyRoleName()
    {
        return $this->userFriendlyRoleName;
    }

}
