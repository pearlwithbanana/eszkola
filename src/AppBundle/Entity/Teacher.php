<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\AppInterface\EntityName;

/**
 * @ORM\Entity
 * @ORM\Table(name="teacher")
 */
class Teacher implements EntityName
{
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\Column(name="teacher_id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_id_user", referencedColumnName="user_id", nullable=false)
     */
    private $user;

    /**
     * @Assert\Valid()
     */
    public $users;

    public function addUsers(User $user)
    {
        $this->users->add($user);
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getEntityName()
    {
        return "Teacher";
    }

    public function getTeacherFullNameAndEmail()
    {
        return $this->user->getFullName()." [".$this->user->getEmail()."]";
    }
}
