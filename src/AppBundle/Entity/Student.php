<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 03.11.15
 * Time: 19:21
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="student")
 */
class Student extends User
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="student_id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Division", inversedBy="divisionName")
     * @ORM\JoinColumn(name="student_class_id", referencedColumnName="class_id", nullable=false)
     */
    private $studentDivision;
    //@TODO grades, subjects, [groups]

    public function getId()
    {
        return $this->id;
    }

    public function setStudentDivision(\AppBundle\Entity\Division $studentDivision)
    {
        $this->studentDivision = $studentDivision;

        return $this;
    }

    public function getStudentDivision()
    {
        return $this->studentDivision;
    }
}
