<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="educator")
 */
class Educator
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="educator_id", type="integer")
     * @ORM\GeneratedValue("AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Teacher", inversedBy="user")
     * @ORM\JoinColumn(name="fk_id_teacher", referencedColumnName="teacher_id", nullable=false)
     */
    private $teacher;

    /**
     * @ORM\OneToOne(targetEntity="Division", inversedBy="divisionName")
     * @ORM\JoinColumn(name="fk_id_division", referencedColumnName="class_id")
     */
    private $division;

    public function getId()
    {
        return $this->id;
    }

    public function setTeacher(\AppBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getTeacher()
    {
        return $this->teacher;
    }

    public function setDivision(\AppBundle\Entity\Division $division = null)
    {
        $this->division = $division;

        return $this;
    }

    public function getDivision()
    {
        return $this->division;
    }
}
