<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use AppBundle\AppInterface\EntityName;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="username", message="Użytkownik o podanej nazwie już istnieje")
 * @UniqueEntity(fields="email", message="Użytkownik o podanym adresie email już istnieje")
 */
class User implements AdvancedUserInterface, \Serializable, EntityName
{

    public function __construct()
    {
        $this->plainPassword = sha1(mt_rand(0, 10));
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(name="user_id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Nie podano adres email")
     * @Assert\Email(message="Podano niewłaściwy format adresu email")
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $username;

    /**
     * @ORM\Column(name="full_name", type="string", nullable=false)
     * @ORM\OneToOne(targetEntity="Teacher", mappedBy="user")
     * @Assert\NotBlank(message="Nie podano imienia i nazwiska")
     * @Assert\Regex(
     *  pattern = "/[a-zA-Z]\s/", message = "Imię i nazwisko musi być rozdzielone spacją, dozwolone są tylko litery"
     * )
     */
    private $fullName;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfBirth;

    /**
     * @Assert\NotBlank(message="Nie podano hasła")
     * @Assert\Length(
     *  min = 8, max = 4096,
     *  minMessage = "Hasło powinno zawierać co najmniej {{ limit }} znaków",
     *  maxMessage = "Hasło jest za długie"
     * )
     */
    private $plainPassword;

    protected $oldPassword;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="role")
     * @ORM\JoinColumn(name="user_role_id", referencedColumnName="role_id")
     */
    private $roles; //id = 6 is the basic user id;

    /**
     * @ORM\Column(name="phone_number", type="integer", nullable=true)
     * @Assert\Length(
     *  min = 9,
     *  max = 9,
     *  exactMessage = "Numer telefonu musi mieć dokładnie 9 znaków"
     * )
     */
    private $phoneNumber = null;

    /**
     * @ORM\Column(name="mail_hash", type="string", nullable=true)
     */
    private $mailHash = null;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    public function getRoles()
    {
        return array($this->roles->getRole());
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    public function serialize()
    {
        return serialize(
            array(
                $this->id,
                $this->username,
                $this->email,
                $this->password,
                $this->isActive,
            )
        );
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->isActive,
            ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }


    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return (bool)$this->getIsActive();
    }


    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function setMailHash($mailHash)
    {
        $this->mailHash = $mailHash;

        return $this;
    }

    public function getMailHash()
    {
        return $this->mailHash;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function SetPlainRoles($roles)
    {
        $this->plainRoles = $roles;

        return $this;
    }

    public function getPlainRoles()
    {
        return $this->plainRoles;
    }

    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function getEntityName()
    {
        return "User";
    }

    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    public function getOldPassword()
    {
        return $this->oldPassword;
    }

}
