<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="first_run_configuration")
 */
class FirstRun
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="current_configuration_step", type="integer", nullable=true)
     */
    private $currentConfigurationStep = 1; //if null, system is already configured

    public function setCurrentConfigurationStep($currentConfigurationStep)
    {
        $this->currentConfigurationStep = $currentConfigurationStep;

        return $this;
    }

    public function getCurrentConfigurationStep()
    {
        return $this->currentConfigurationStep;
    }
}
