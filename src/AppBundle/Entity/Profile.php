<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 03.11.15
 * Time: 20:17
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="profile")
 */
class Profile
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="profile_id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @ORM\Column(name="profile_name", type="string", nullable=false)
     * @ORM\OneToMany(targetEntity="Division", mappedBy="profile")
     */
    private $profileName;

    public function getId()
    {
        return $this->id;
    }

    public function setProfileName($profileName)
    {
        $this->profileName = $profileName;

        return $this;
    }

    public function getProfileName()
    {
        return $this->profileName;
    }
}
