<?php
/**
 * Created by PhpStorm.
 * User: pearlwithbanana
 * Date: 31.10.15
 * Time: 23:07
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="class")
 * @UniqueEntity("divisionName")
 */
class Division
{
    public function __construct() {
        $this->divisions = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\Column(name="class_id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="class_name", type="string", length=4, nullable=false)
     * @ORM\OneToOne(targetEntity="Educator", mappedBy="division")
     */
    private $divisionName;

    /**
     * @ORM\OneToOne(targetEntity="Educator", inversedBy="teacher")
     * @ORM\JoinColumn(name="fk_id_educator", referencedColumnName="educator_id", nullable=true)
     * @ORM\OneToMany(targetEntity="Student", mappedBy="division")
     */
    private $educator = null;

    /**
     * @ORM\ManyToOne(targetEntity="Profile", inversedBy="profileName")
     * @ORM\JoinColumn(name="class_profile_id", referencedColumnName="profile_id", nullable=true)
     */
    private $profile = null;
    /*
     * future @TODO:
     * if the HTML validation is disabled, user is able to TRY send empty data to the database.
     * Unless the database nullable property is set to false it isn't dangerous.
     * Thus Length(min) validation is omitted. The Length(max) property works fine and shows
     * errors in the form (as I said, the validation constraints are called only if HTML validation
     * is for some reasons disabled)
     * Dunno why it bahaves like this.
     * Total hours spent on TRYING to understand it: 5
     */

    /**
     * @Assert\All(constraints = {
     * @Assert\NotBlank(),
     * @Assert\Length(min=1, max=4,
     * minMessage="Nazwa klasy musi mieć co najmnie jeden znak",
     * maxMessage="Nazwa klasy nie może mieć więcej niż {{ limit }} znaki")
     * })
     */
    public $divisions;

    public function removeDivision($divisionName)
    {
        $this->divisions->removeElement($divisionName);
    }

    public function getDivisions() {
        return $this->divisions;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDivisionName($divisionName)
    {
        $this->divisionName = $divisionName;

        return $this;
    }

    public function getDivisionName()
    {
        return $this->divisionName;
    }

    public function setEducator(\AppBundle\Entity\Educator $educator)
    {
        $this->educator = $educator;

        return $this;
    }

    public function getEducator()
    {
        return $this->educator;
    }

    public function setProfile(\AppBundle\Entity\Profile $profile)
    {
        $this->profile = $profile;

        return $this;
    }

    public function getProfile()
    {
        return $this->profile;
    }
}
