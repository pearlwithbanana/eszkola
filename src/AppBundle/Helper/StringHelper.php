<?php

namespace AppBundle\Helper;

class StringHelper
{
    public static function ReplaceDiacritics($string) {
        $characters = array("ą" => "a", "ć" => "c", "ę" => "e", "ł" => "l", "ń" => "n", "ó" => "o", "ś" => "s", "ź" => "z", "ż" => "z");
        foreach ($characters as $key => $value) {
            $string = str_replace($key, $value, $string);
        }

        return $string;
    }
}