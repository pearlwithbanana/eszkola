<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
//use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\HttpKernel;

class ResponseListener
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function onIsSystemConfigured(GetResponseEvent $event)
    {
        //get current configuration url
        $nextStepService = $this->container->get('next_configuration_step');
        $url = $nextStepService->getCurrentConfigurationStepUrl();
        //get current rote
        $currentConfigurationStep = $nextStepService->getCurrentStep();
        if ($currentConfigurationStep === null) { //if system is configured
            return;
        }

        $request = $event->getRequest();
        $route = $request->getPathInfo();

        if ($route === $url) {
            return;
        }

        //Check if user is authenticated
        if ($route === "/login") {
            return;
        }

        $securityContext = $this->container->get('security.authorization_checker');

        if(!$securityContext->isGranted("IS_AUTHENTICATED_FULLY")) {
            $response = new RedirectResponse('login');
            return $event->setResponse($response);
        }

        //if the user is authenticated and has not ROOT role, throw access danied exception

        if(!$securityContext->isGranted('ROLE_ROOT')) {
            throw new AccessDeniedHttpException();
        }


        //redirect to current configuration route
        else {
            $response = new RedirectResponse($url);
            return $event->setResponse($response);
        }
    }
}

