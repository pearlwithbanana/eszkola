<?php

namespace AppBundle\Roles;

use Doctrine\ORM\EntityManager;

class UserRoles
{
    private $userRolesMappedArray;
    private $entityManager;
    private $availableRoles;

    public function __construct(EntityManager $em, $userType) {
        $this->entityManager = $em;
    }

    public function findAllRoles($userType)
    {
        if ($userType === "ROLE_ROOT" || $userType === "ROLE_HEADMASTER") {
            return
                $this
                    ->entityManager
                    ->getConnection()
                    ->executeQuery(
                        "SELECT role_name FROM user_role WHERE required_level_to_add = 0 AND required_level_to_add = 1 ORDER BY role_id;"
                    )
                    ->fetchAll();
        } else if ($userType === "ROLE_ROOT" || $userType === "ROLE_SECRETARY") {
            return $this
                ->entityManager
                ->getConnection()
                ->executeQuery(
                    "SELECT role_name FROM user_role WHERE required_level_to_add = 0 ORDER BY role_id;"
                )
                ->fetchAll();
        } else {
            return null;
        }
    }

   public function getAvailableRoles() {
       return $this->availableRoles;
   }

    public function FillAvailableUserRoles() {
        $this->userRolesMappedArray = \AppBundle\Helper\DatabaseHelper::QueryToTheDatabase($query);
    }
}
