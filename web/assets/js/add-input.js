var collectionCount = 0;

$(document).ready(function() {
    $('#add-another-input').click(function(e) {
        e.preventDefault();
        var collection = $('#fields-list');
        // grab the prototype template
        var newWidget = collection.attr('data-prototype');
        // replace the "__name__" used in the id and name of the prototype
        // with a number that's unique to your emails
        // end name attribute looks like name="contact[emails][2]"
        newWidget = newWidget.replace(/__name__/, collectionCount);
        if(collectionCount != 0) {
            newWidget += '<button data-field-button="remove" type="button">Usuń</button>';
        }
        collectionCount++;

        // create a new list element and add it to the list
        var newLi = $('<li></li>').html(newWidget);
        newLi.appendTo(collection);
        // get the remove button if exisist and add it to the new <li> row
        addRemoveFieldScript();
    });
});

//document is modified dynamically so it's needed to add a script to remove an input field every time
// it is generated. The above function is called every time the #add-another-input button is clicked
function addRemoveFieldScript() {
    var removeScript = document.createElement('script');
    removeScript.setAttribute('src', 'assets/js/remove-input.js');
    document.body.appendChild(removeScript);
}

$(document).ready(function () {
    $("#add-another-input").click();
});

